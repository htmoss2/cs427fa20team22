<%@taglib prefix="itrust" uri="/WEB-INF/tags.tld"%>
<%@page errorPage="/auth/exceptionHandler.jsp"%>

<%@page import="edu.ncsu.csc.itrust.action.AddPreRegisteredPatientAction"%>
<%@page import="edu.ncsu.csc.itrust.BeanBuilder"%>
<%@page import="edu.ncsu.csc.itrust.beans.PatientBean"%>
<%@page import="edu.ncsu.csc.itrust.beans.forms.HealthRecordForm"%>
<%@page import="edu.ncsu.csc.itrust.exception.FormValidationException"%>
<%@page import="edu.ncsu.csc.itrust.exception.ITrustException"%>

<%@include file="/global.jsp" %>

<%
pageTitle = "iTrust - PreRegister Patient";
%>

<%@include file="/header.jsp" %>

<%
boolean formIsFilled = request.getParameter("formIsFilled") != null && request.getParameter("formIsFilled").equals("true");

if (formIsFilled) {
    //This page is not actually a "page", it just adds a user and forwards.
    
    PatientBean p = new BeanBuilder<PatientBean>().build(request.getParameterMap(), new PatientBean());
    HealthRecordForm h = new BeanBuilder<HealthRecordForm>().build(request.getParameterMap(), new HealthRecordForm());
            
    String pwd = request.getParameter("password");
    String VerifyPwd = request.getParameter("verifyPassword");

    if (pwd.equals(VerifyPwd)){
        
        try{
            long newMID = 021700L; 
            
            newMID = new AddPreRegisteredPatientAction(DAOFactory.getProductionInstance(), newMID).addPatient(p, h);
            
            session.setAttribute("pid", Long.toString(newMID));
            String fullname;
            String password;
            password = p.getPassword();
            fullname = p.getFullName();
            
            loggingAction.logEvent(TransactionType.PATIENT_CREATE, newMID, newMID, "");
%>
        <div align=center>
            <span class="iTrustMessage">New Pre-registered Prepatient <%= StringEscapeUtils.escapeHtml("" + (fullname)) %> successfully added!</span>
            <br /><br />
            <table class="fTable">
                <tr>
                    <th colspan="2">New Pre-registered Patient Information</th>
                </tr>
                <tr>
                    <td class="subHeaderVertical">MID:</td>
                    <td id="newMID"><%= StringEscapeUtils.escapeHtml("" + (newMID)) %></td>
                </tr>
            </table>
            <br />Please get this information to <b><%= StringEscapeUtils.escapeHtml("" + (fullname)) %></b>
        </div>       
<%
        } catch(FormValidationException e) {
%>
        <div align=center>
            <span class="iTrustError"> <%= StringEscapeUtils.escapeHtml(e.getMessage()) %></span>
            <!-- StringEscapeUtils.escapeHtml(e.getMessage())  -->
        </div>
<%
        } catch (ITrustException ex) {
%>
        <div align=center>
            <span class="iTrustError"> <%= StringEscapeUtils.escapeHtml(ex.getMessage()) %></span>
            <!-- StringEscapeUtils.escapeHtml(ex.getMessage())  -->
        </div>
<%
        }   
    }else{
%>        
<script type="text/javascript">
    
            alert("Passwords do not match! Pre-Registration Not Successed!");
            document.getElementById('submit_preregister').disabled = true;

</script>
<%    
    }
}
%>

<div align=center>
<form action="PreRegisterPatient.jsp" method="post">                 <!--   Which page DIRECT  to     -->

    <input type="hidden" name="formIsFilled" value="true"> <br />
<br />
<div style="width: 50%; text-align:left;">Please enter in the name of the Pre-registered
patient, with a valid email address. If the user does not have an email
address, use the hospital&#39;s email address, [insert pre-defined email],
to recover the password.</div>
<br />
<br />
<table class="fTable">
	<tr>
		<th colspan=3 style="text-align:center">Pre-registered Patient Information</th>
	</tr>
	<tr>
		<td class="subHeaderVertical">First name:</td>
		<td><input type="text" name="firstName" required></td>
		<td style="font-size: 16pt; text-align:left; color:red">*</td>
	</tr>
	<tr>
		<td class="subHeaderVertical">Last Name:</td>
		<td><input type="text" name="lastName" required></td>
		<td style="font-size: 16pt; text-align:left; color:red">*</td>
	</tr>
	<tr>
		<td class="subHeaderVertical">Email:</td>
		<td><input type="text" name="email" required></td>
		<td style="font-size: 16pt; text-align:left; color:red">*</td>
	</tr>
	<tr>
		<td class="subHeaderVertical">Password:</td>
		<td><input type="password" name="password" id ="password" required></td>
		<td style="font-size: 16pt; text-align:left; color:red">*</td>
	</tr>
	<tr>
		<td class="subHeaderVertical">Verify Password:</td>
		<td><input type="password" name="verifyPassword" id = "verifyPassword" required></td>
		<td style="font-size: 16pt; text-align:left; color:red">*</td>
	</tr>
    <tr>
		<td class="subHeaderVertical">Address:</td>
		<td>   <input name="streetAddress1" type="text"><br />
		<input name="streetAddress2" type="text"></td>
		<td> </td>
	</tr>
	<tr>
		<td class="subHeaderVertical">City:</td>
		<td>   <input name="city" type="text"></td>
		<td> </td>
	</tr>
	<tr>
		<td class="subHeaderVertical">State:</td>
		<td><itrust:state name="state" value="AK" /></td>
		<td> </td>
	</tr>
	<tr>
		<td class="subHeaderVertical">Zip:</td>
	    <td>   <input type="text" name="zip" maxlength="10" size="10"></td>
	    <td> </td>
	</tr>
	<tr>
		<td class="subHeaderVertical">Phone:</td>
		<td>   <input type="text" name="phone" size="12" maxlength="12"></td>
		<td> </td>
	</tr>
	<tr>
		<td class="subHeaderVertical">Height:</td>
		<td><input type="text" name="height"></td>
		<td> </td>
	</tr>
	<tr>
		<td class="subHeaderVertical">Weight:</td>
		<td><input type="text" name="weight"></td>
		<td> </td>
	</tr>
	<tr>
		<td class="subHeaderVertical">Smoker:</td>
		<td><input type="radio" id="smoker_yes" name="isSmoker" value="1">
            <label for="smoker_yes">Yes</label>
            <br />
            <input type="radio" id="smoker_no" name="isSmoker" value="0" checked">
            <label for="smoker_no">No</label><br />
        </td>
        <td> </td>
	</tr>
	
	<br/>
	<tr>
		<th colspan=3 style="text-align:center">Insurance Information</th>
	</tr>
			
	<tr>
		<td class="subHeaderVertical">Insurance Provider Name:</td>
		<td>   <input type="text" name="icName"></td>
		<td> </td>
	</tr>
	<tr>
		<td class="subHeaderVertical">Insurance Provider Address:</td>
		<td><input name="icAddress1" type="text"><br />
		<input name="icAddress2" type="text"></td>
		<td> </td>
	</tr>
		<tr>
		<td class="subHeaderVertical">City:</td>
		<td>   <input name="icCity" type="text"></td>
		<td> </td>
	</tr>
	<tr>
		<td class="subHeaderVertical">State:</td>
		<td><itrust:state name="icState" value="AK" /></td>
		<td> </td>
	</tr>
	<tr>
		<td class="subHeaderVertical">Zip:</td>
	    <td>   <input type="text" name="icZip" maxlength="10" size="10"></td>
	    <td> </td>
	</tr>
	<tr>
		<td class="subHeaderVertical">Insurance Provider Phone:</td>
		<td><input type="text" name="icPhone"></td>
		<td> </td>
	</tr>
</table>
<br />

<input type="submit" style="font-size: 16pt; font-weight: bold;" value="Patient Pre-Register" id="submit_preregister">
</form>
<br />
</div>


<%@include file="/footer.jsp" %>
