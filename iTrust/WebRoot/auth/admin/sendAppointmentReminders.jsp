
<%@taglib prefix="itrust" uri="/WEB-INF/tags.tld"%>
<%@page errorPage="/auth/exceptionHandler.jsp"%>
<%@page import="edu.ncsu.csc.itrust.action.SendRemindersAction" %>
<%@page import="edu.ncsu.csc.itrust.exception.ITrustException" %>
<%@page import="java.lang.NumberFormatException" %>

<%@include file="/global.jsp" %>

<%
pageTitle = "iTrust - Send Reminder Message";
%>

<%@include file="/header.jsp" %>

<%
	String input = request.getParameter("withinDays"); 
	if (input != null && !input.equals("")) {
		try {
            int days = Integer.valueOf(input);
            if (days <= 0) {
%>
                <span class="iTrustError"><%=StringEscapeUtils.escapeHtml("Provide a positive number") %></span>
<%
            }
            else {
                SendRemindersAction action = new SendRemindersAction(prodDAO, loggedInMID.longValue());
			    action.sendReminderForAppointments(days);
%>
			    <span class="iTrustMessage"><%=StringEscapeUtils.escapeHtml("Reminders were successfully sent") %></span>
<%		
            }
			
		} catch (NumberFormatException | ITrustException e) {
%>
			<span class="iTrustError"><%=StringEscapeUtils.escapeHtml("Reminders failed to send. Please provide a positive number") %></span>
<%
		}
	}
%>

<div class="page-header"><h1>Send Reminder Message</h1></div>

<form method="post">
<h4> Send reminders to all patients with an appointment within the next n days. Provide n: </h4>
<input type="text" name="withinDays"></td>
<input type="submit" style="font-size: 12pt; font-weight: bold;" value="Send Appointment Reminders">
</form>

<%@include file="/footer.jsp" %>