<%@taglib uri="/WEB-INF/tags.tld" prefix="itrust"%>
<%@page errorPage="/auth/exceptionHandler.jsp"%>

<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.sql.Timestamp"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.TreeMap"%>
<%@page import="java.lang.String"%>
<%@page import="edu.ncsu.csc.itrust.exception.FormValidationException"%>
<%@page import="edu.ncsu.csc.itrust.enums.TransactionType"%>
<%@page import="edu.ncsu.csc.itrust.dao.DAOFactory"%>
<%@page import="edu.ncsu.csc.itrust.beans.TransactionBean"%>
<%@page import="edu.ncsu.csc.itrust.beans.TransactionWithRoleBean"%>
<%@page import="org.apache.commons.lang.StringEscapeUtils"%>
<%@page import="edu.ncsu.csc.itrust.enums.TransactionType"%>
<%@page import="edu.ncsu.csc.itrust.enums.Role"%>


<%@include file="/global.jsp" %>

<%
pageTitle = "iTrust - View Transaction Logs";
String view = request.getParameter("viewSelect");
%>

<%@include file="/header.jsp" %>
<%
	//get form data
    String startDate = request.getParameter("startDate");
    String endDate = request.getParameter("endDate");
	String mainRole = request.getParameter("mainRole");
	String secondRole = request.getParameter("secondRole");
    String transactionType = request.getParameter("transactionType");
	String convertedDate = request.getParameter("convertedDate");
	SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");
	String formatted = df.format(new Date());

    if (startDate == null)
    	startDate = "01/01/2000";
    if (endDate == null)
    	endDate = formatted;
%>
<br />
<form action="viewTransactionLogs.jsp" method="post" id="formMain">
<input type="hidden" name="viewSelect" value=""/>
<table class="fTable" align="center" id="transactionLogsTable">
	<tr>
		<th colspan="4">View Transaction Logs</th>
	</tr>
    	<tr class="subHeader">
		<td>Start Date:</td>
		<td>
			<input name="startDate" value="<%= StringEscapeUtils.escapeHtml("" + (startDate)) %>" size="10">
            <input type=button value="Select Date" onclick="displayDatePicker('startDate');">
		</td>
		<td>End Date:</td>
		<td>
			<input name="endDate" value="<%= StringEscapeUtils.escapeHtml("" + (endDate)) %>" size="10">
            <input type=button value="Select Date" onclick="displayDatePicker('endDate');">
		</td>
	</tr>
	<tr class="subHeader">
		<td>Main Role:</td>
		<td>
			<select name="mainRole" style="font-size:10" >
				<option value="All"> All</option>
				<%
				for(Role type : Role.values()){
					%><option value="<%= StringEscapeUtils.escapeHtml("" + (type.getUserRolesString()))%>"><%=type.getUserRolesString()%></option><%   
				}
				%>
			</select>
		</td>
		<td>Secondary Role:</td>
		<td>
			<select name="secondRole" style="font-size:10" >
				<option value="All"> All</option>
				<%
				for(Role type : Role.values()){
					%><option value="<%= StringEscapeUtils.escapeHtml("" + (type.getUserRolesString()))%>"><%=type.getUserRolesString()%></option><%   
				}
				%>
				<option value="N/A"> N/A</option>
			</select>
		</td>    
	</tr>
	<tr class="subHeader">
		<td colspan="2" style="text-align: left;">Transaction Type:</td>
		<td>
			<select name="transactionType" style="font-size:10" >
				<option value="All"> All</option>
				<%
				for(TransactionType type : TransactionType.values()){
					%><option value="<%= StringEscapeUtils.escapeHtml("" + (type.name()))%>"><%=type.name()%>
					</option><%   
				}
				%>
			</select>
		</td>
		<td></td>
	</tr>
	<tr>
		<td colspan="1" style="text-align: center;"><input type="submit" id="viewReport" value="View Report" onclick="document.forms[0].elements['viewSelect'].value = 'view report clicked';return true;"></td>
		<td colspan="1" style="text-align: center;"><input type="submit" id="viewGraph" value="Summarize" onclick="document.forms[0].elements['viewSelect'].value = 'summarize clicked';return true;"></td>
		<td> </td>
		<td colspan="1" style="text-align: center;"><button><a href="viewTransactionLogs.jsp?reset=true" style="text-decoration:none; padding: 0.4em;color:black;">Reset</a></button></td>

	</tr>
</table>
</form>
<br />
<%

String resetCheck = null;
if(request.getParameter("reset")!= null){
	resetCheck = request.getParameter("reset");
}

if (view != null && view.equals("view report clicked") && resetCheck != "true") { %>
	<table border=1 align="center">	
		<%
		List<TransactionWithRoleBean> list = DAOFactory.getProductionInstance().getTransactionDAO().getAllTransactionsWithRoles();
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
		Date convertedSimpleDate = null;
		Date startSimpleDate = null;
		Date endSimpleDate = null;
		
		if(startDate != ""){
			startSimpleDate = sdf.parse(startDate);
		}
		else{
			startSimpleDate = sdf.parse("01/01/2000");
		}
		if(endDate != ""){
			endSimpleDate = sdf.parse(endDate);
		}
		else{
			endSimpleDate = sdf.parse(formatted);
		}

		boolean dateFormatError = false;
		String regex = "^(1[0-2]|0[1-9])/(3[01]|[12][0-9]|0[1-9])/[0-9]{4}$";
		boolean result = (startDate.matches(regex)||startDate.equals("")) && (endDate.matches(regex)||endDate.equals(""));
		if(!result) {
			dateFormatError = true;
			%>
			<span class="iTrustError">ERROR: Date must by in the format: MM/dd/yyyy</span>
			<%
		} 
		else if(endSimpleDate.before(startSimpleDate)){
			dateFormatError = true;
			%>
			<span class="iTrustError">ERROR: End Date cannot be before Start Date</span>
			<%
		}

		int countEntries = 0;
		if(!dateFormatError){
			%>
				<tr>
					<th>Main Role</th>
					<th>Secondary Role</th>
					<th>Type</th>
					<th>Extra Info</th>
					<th>Time Logged</th>
				</tr>
			<%
			for (TransactionWithRoleBean t : list) {
				
				convertedSimpleDate = sdf.parse(StringEscapeUtils.escapeHtml("" + (t.getConvertedDate())));

				if(StringEscapeUtils.escapeHtml("" + (t.getMainRole())).equals(StringEscapeUtils.escapeHtml(mainRole))
												|| StringEscapeUtils.escapeHtml(mainRole).equals("All")){
					if(StringEscapeUtils.escapeHtml("" + (t.getSecondaryRole())).equals(StringEscapeUtils.escapeHtml(secondRole))
													|| StringEscapeUtils.escapeHtml(secondRole).equals("All")){
						if(StringEscapeUtils.escapeHtml("" + (t.getTransactionType().name())).equals(StringEscapeUtils.escapeHtml	(transactionType)) || StringEscapeUtils.escapeHtml(transactionType).equals("All")){
							if((convertedSimpleDate.before(endSimpleDate) || convertedSimpleDate.equals(endSimpleDate)) && (convertedSimpleDate.after(startSimpleDate) || convertedSimpleDate.equals(startSimpleDate))){
								countEntries++;
								%>
								<tr id="resultRow">
									<td id="mainTD"><%= StringEscapeUtils.escapeHtml("" + (t.getMainRole())) %></td>
									<td id="secondTD"><%= StringEscapeUtils.escapeHtml("" + (t.getSecondaryRole())) %></td>
									<td id="typeTD"><%= StringEscapeUtils.escapeHtml("" + (t.getTransactionType().name())) %></td>
									<td id="infoTD"><%= StringEscapeUtils.escapeHtml("" + (t.getAddedInfo())) %></td>
									<td id="timeTD"><%= StringEscapeUtils.escapeHtml("" + (t.getTimeLogged())) %></td>
								</tr>
								<%
							}
						}
					}
				}
			}

			if(countEntries == 0){
			%>
				<tr id="resultRow">
					<td style="color:red;" id="noResults" colspan="5">There are no results for the criteria specified</td>
				</tr>
			<%
			}
		}
		%>
	</table>

<% } %>

<% 
if (view != null && view.equals("summarize clicked") && resetCheck != "true") { 
	List<TransactionWithRoleBean> list = DAOFactory.getProductionInstance().getTransactionDAO().getAllTransactionsWithRoles();
	List<TransactionWithRoleBean> filteredList = new ArrayList();
	SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
	Date convertedSimpleDate = null;
	Date startSimpleDate = null;
	Date endSimpleDate = null;
	if(startDate != ""){
		startSimpleDate = sdf.parse(startDate);
	}
	else{
		startSimpleDate = sdf.parse("01/01/2000");
	}
	if(endDate != ""){
		endSimpleDate = sdf.parse(endDate);
	}
	else{
		endSimpleDate = sdf.parse(formatted);
	}
	
	boolean dateFormatError = false;
		String regex = "^(1[0-2]|0[1-9])/(3[01]|[12][0-9]|0[1-9])/[0-9]{4}$";
		boolean result = (startDate.matches(regex)||startDate.equals("")) && (endDate.matches(regex)||endDate.equals(""));
		if(!result) {
			dateFormatError = true;
			%>
			<span class="iTrustError">ERROR: Date must by in the format: MM/dd/yyyy</span>
			<%
		} 
		else if(endSimpleDate.before(startSimpleDate)){
			dateFormatError = true;
			%>
			<span class="iTrustError">ERROR: End Date cannot be before Start Date</span>
			<%
		}
	%>
	<h2 align="center" >Summary of Transactions</h2>
	<%
	
	for (TransactionWithRoleBean t : list) {
				
		convertedSimpleDate = sdf.parse(StringEscapeUtils.escapeHtml("" + (t.getConvertedDate())));

		if(StringEscapeUtils.escapeHtml("" + (t.getMainRole())).equals(StringEscapeUtils.escapeHtml(mainRole))
										|| StringEscapeUtils.escapeHtml(mainRole).equals("All")){
			if(StringEscapeUtils.escapeHtml("" + (t.getSecondaryRole())).equals(StringEscapeUtils.escapeHtml(secondRole))
											|| StringEscapeUtils.escapeHtml(secondRole).equals("All")){
				if(StringEscapeUtils.escapeHtml("" + (t.getTransactionType().name())).equals(StringEscapeUtils.escapeHtml	(transactionType)) || StringEscapeUtils.escapeHtml(transactionType).equals("All")){
					if((convertedSimpleDate.before(endSimpleDate) || convertedSimpleDate.equals(endSimpleDate)) && (convertedSimpleDate.after(startSimpleDate) || convertedSimpleDate.equals(startSimpleDate))){
						filteredList.add(t);
					}
				}
			}
		}
	}
	
	if (filteredList.isEmpty()) {
		%>
			<h4 align="center" style="color:red;" id="noResults">There are no results for the criteria specified</h4>
		<%
	}
	else {
		HashMap<String, Integer> mainRoleMap = new HashMap<String, Integer>();
		HashMap<String, Integer> secondaryRoleMap = new HashMap<String, Integer>();
		HashMap<String, Integer> transactionMonthMap = new HashMap<String, Integer>();
		HashMap<String, Integer> transactionTypeMap = new HashMap<String, Integer>();
		
		for (TransactionWithRoleBean tb : filteredList) {
			String key = tb.getMainRole();
			String key2 = tb.getSecondaryRole();
			String key3 = tb.getTimeLogged().toString();
			key3 = key3.substring(0, 7);
			String key4 = tb.getTransactionType().getDescription();
			
			if (mainRoleMap.get(key) == null) {
				mainRoleMap.put(key, 1);
			} else {
				mainRoleMap.put(key, mainRoleMap.get(key) + 1);
			}
			
			if (secondaryRoleMap.get(key2) == null) {
				secondaryRoleMap.put(key2, 1);
			} else {
				secondaryRoleMap.put(key2, secondaryRoleMap.get(key2) + 1);
			}
			
			if (transactionMonthMap.get(key3) == null){
				transactionMonthMap.put(key3, 1);
			} else {
				transactionMonthMap.put(key3, transactionMonthMap.get(key3) + 1);
			}
			
			if (transactionTypeMap.get(key4) == null) {
				transactionTypeMap.put(key4, 1);
			} else {
				transactionTypeMap.put(key4, transactionTypeMap.get(key4) + 1);
			}
		}

		TreeMap<String, Integer> sortedMonthMap = new TreeMap<>();
		sortedMonthMap.putAll(transactionMonthMap);

		%>
		<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
		<script type="text/javascript">
			
			google.charts.load('current', {'packages':['corechart']});
			
			google.charts.setOnLoadCallback(drawGraphs);
			
			function drawGraphs() {
			
				var data = new google.visualization.DataTable();
				data.addColumn('string', 'MainRole');
				data.addColumn('number', 'Count');
				
				var data2 = new google.visualization.DataTable();
				data2.addColumn('string', 'SecondaryRole');
				data2.addColumn('number', 'Count');
				
				var data3 = new google.visualization.DataTable();
				data3.addColumn('string', 'Month');
				data3.addColumn('number', 'Count');
				
				var data4 = new google.visualization.DataTable();
				data4.addColumn('string', 'TransactionType');
				data4.addColumn('number', 'Count');
				
				<%for (String key : mainRoleMap.keySet()) {%>
					data.addRows([['<%=key%>', <%=mainRoleMap.get(key)%>]]);
				<%}%>
				
				<%for (String key : secondaryRoleMap.keySet()) {%>
					data2.addRows([['<%=key%>', <%=secondaryRoleMap.get(key)%>]]);
				<%}%>
				
				<%for (String key : sortedMonthMap.keySet()) {%>
					data3.addRows([['<%=key%>', <%=sortedMonthMap.get(key)%>]]);
				<%}%>
				
				<%for (String key : transactionTypeMap.keySet()) {%>
					data4.addRows([['<%=key%>', <%=transactionTypeMap.get(key)%>]]);
				<%}%>
				
				var options = {'title':'Number of Transactions per Main Role',
							'width':600,
							'height':500,
							'legend' : 'none'};
				
				var options2 = {'title':'Number of Transactions per Secondary Role',
						'width':600,
						'height':500,
							'legend' : 'none'};
				
				var options3 = {'title':'Number of Transactions per Month',
						'width':600,
						'height':500,
						'legend' : 'none'};
				
				var options4 = {'title':'Number of Transactions per Transaction Type',
						'width':600,
						'height':500,
						'legend' : 'none'};
				
				var chart = new google.visualization.ColumnChart(document.getElementById('chart_div'));
				var chart2 = new google.visualization.ColumnChart(document.getElementById('chart_div2'));
				var chart3 = new google.visualization.ColumnChart(document.getElementById('chart_div3'));
				var chart4 = new google.visualization.ColumnChart(document.getElementById('chart_div4'));
				chart.draw(data, options);
				chart2.draw(data2, options2);
				chart3.draw(data3, options3);
				chart4.draw(data4, options4);
			}
		</script>
	<table id="Graph_table" class="fTable">
		<tr>
			<td>
				<div id="chart_div"></div>
			</td>
			<td>
				<div id="chart_div2"></div>
			</td>
		</tr>
		<tr>
			<td>
				<div id="chart_div3"></div>
			</td>
			<td>
				<div id="chart_div4"></div>
			</td>
		</tr>
	</table>
	<%
	}
}
%>

<br />

<%@include file="/footer.jsp" %>