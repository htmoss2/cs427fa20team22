<%@page import="java.text.ParseException"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="edu.ncsu.csc.itrust.action.EditApptAction"%>
<%@page import="edu.ncsu.csc.itrust.action.EditApptTypeAction"%>
<%@page import="edu.ncsu.csc.itrust.beans.ApptBean"%>
<%@page import="edu.ncsu.csc.itrust.beans.ApptTypeBean"%>
<%@page import="edu.ncsu.csc.itrust.dao.mysql.ApptTypeDAO"%>
<%@page errorPage="/auth/exceptionHandler.jsp"%>
<%@page import="java.util.List"%>
<%@page import="edu.ncsu.csc.itrust.action.ViewMyApptsAction"%>
<%@page import="edu.ncsu.csc.itrust.exception.FormValidationException"%>
<%@page import="edu.ncsu.csc.itrust.exception.ITrustException"%>

<%@include file="/global.jsp"%>

<%
	pageTitle = "iTrust - Appointment Requests";
%>
<%@include file="/header.jsp"%>
<%
	EditApptAction editAction = new EditApptAction(prodDAO, loggedInMID.longValue());
	ApptTypeDAO apptTypeDAO = prodDAO.getApptTypeDAO();
	String msg = "";
	long hcpid = 0L;
	String comment = "";
	String date = "";
	String hourI = "";
	String minuteI = "";
	String tod = "";
	String apptType = "";
	String prompt = "";

	ApptBean original = null;
	String aptParameter = "";
	if (request.getParameter("apt") != null) {
		aptParameter = request.getParameter("apt");
		try {
			int apptID = Integer.parseInt(aptParameter);
			original = editAction.getAppt(apptID);
			if (original == null){
				response.sendRedirect("viewMyAppts.jsp");
			}
		} catch (NullPointerException npe) {
			response.sendRedirect("viewMyAppts.jsp");
		} catch (NumberFormatException e) {
			// Handle Exception
			response.sendRedirect("viewMyAppts.jsp");
		}
	} else {
		response.sendRedirect("viewMyAppts.jsp");
	}
	
	Long patientID = 0L;
	if (session.getAttribute("pid") != null) {
		String pidString = (String) session.getAttribute("pid");
		patientID = Long.parseLong(pidString);
		try {
			editAction.getName(patientID);
		} catch (ITrustException ite) {
			patientID = 0L;
		}
	}
	
	boolean hideForm = false;
	boolean error = false;
	String hidden = "";

	Date oldDate = new Date(original.getDate().getTime());
	DateFormat dFormat = new SimpleDateFormat("MM/dd/yyyy");
	DateFormat tFormat = new SimpleDateFormat("hhmma");
	String hPart = tFormat.format(oldDate).substring(0,2);
	String mPart = tFormat.format(oldDate).substring(2,4);
	String aPart = tFormat.format(oldDate).substring(4);
	
	String lastSchedDate=dFormat.format(oldDate);
	String lastApptType=original.getComment();
	String lastTime1=hPart;
	String lastTime2=mPart;
	String lastTime3=aPart;
	String lastComment=original.getComment();
	if(lastComment == null) lastComment="";

	if (request.getParameter("editAppt") != null && request.getParameter("apptID") != null) {
		String headerMessage = "";
	    if (request.getParameter("editAppt").equals("Remove")) {
			// Delete the appointment
			ApptBean appt = new ApptBean();
			appt.setApptID(Integer.parseInt(request.getParameter("apptID")));
			headerMessage = editAction.removeAppt(appt);
			if(headerMessage.startsWith("Success")) {
				hideForm = true;
				session.removeAttribute("pid");
				loggingAction.logEvent(TransactionType.APPOINTMENT_REMOVE,
				loggedInMID.longValue(), original.getPatient(), ""+original.getApptID());
%>
				<div align=center>
					<span class="iTrustMessage"><%=StringEscapeUtils.escapeHtml(headerMessage)%></span>
				</div>
<%
			} else {
%>
				<div align=center>
					<span class="iTrustError"><%=StringEscapeUtils.escapeHtml(headerMessage)%></span>
				</div>
<%
			}
		} 
	}
%>
<h1>Cancel Existing Appointment</h1>
<%
	if (msg.contains("ERROR")) {
%>
		<span class="iTrustError"><%=msg%></span>
<%
	} else {
%>
		<span class="iTrustMessage"><%=msg%></span>
<%
	}
%>
<%=prompt%>
<%
	Date d = new Date(original.getDate().getTime());
	DateFormat format = new SimpleDateFormat("MM/dd/yyyy hh:mm a");
%>
<div>
	<h4>The following appointment will be canceled by pressing the "Cancel Appointment" button.</h4>
	<p><b>Patient:</b> <%= StringEscapeUtils.escapeHtml("" + ( editAction.getName(original.getPatient()) )) %></p>
	<p><b>HCP:</b> <%= StringEscapeUtils.escapeHtml("" + ( editAction.getName(original.getHcp()) )) %></p>
	<p><b>Type:</b> <%= StringEscapeUtils.escapeHtml("" + ( original.getApptType() )) %></p>
	<p><b>Date/Time:</b> <%= StringEscapeUtils.escapeHtml("" + ( format.format(d) )) %></p>
	<p><b>Duration:</b> <%= StringEscapeUtils.escapeHtml("" + ( apptTypeDAO.getApptType(original.getApptType()).getDuration()+" minutes" )) %></p>
</div>
<%
	if(!hideForm){
%>
		<form id="mainForm" type="hidden" method="post" action="cancelApptPatient.jsp?apt=<%=aptParameter %>&apptID=<%=original.getApptID() %>">
			<input type="submit" value="Cancel Appointment" name="editApptButton" id="removeButton" onClick="document.getElementById('editAppt').value='Remove';"/>
			<input type="hidden" id="editAppt" name="editAppt" value=""/>
		</form>
<%
		}
%>
<%@include file="/footer.jsp"%>
