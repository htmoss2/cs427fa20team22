<%@page import="java.text.ParseException"%>
<%@page import="java.sql.Timestamp"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="edu.ncsu.csc.itrust.action.EditApptAction"%>
<%@page import="edu.ncsu.csc.itrust.beans.ApptBean"%>
<%@page import="edu.ncsu.csc.itrust.beans.ApptTypeBean"%>
<%@page import="edu.ncsu.csc.itrust.dao.mysql.ApptTypeDAO"%>
<%@page import="edu.ncsu.csc.itrust.beans.HCPVisitBean"%>
<%@page import="edu.ncsu.csc.itrust.action.ViewVisitedHCPsAction"%>
<%@page errorPage="/auth/exceptionHandler.jsp"%>
<%@page import="edu.ncsu.csc.itrust.action.AddApptRequestAction"%>
<%@page import="edu.ncsu.csc.itrust.beans.ApptRequestBean"%>
<%@page import="java.util.List"%>
<%@page import="edu.ncsu.csc.itrust.action.ViewMyApptsAction"%>
<%@page import="edu.ncsu.csc.itrust.exception.FormValidationException"%>
<%@page import="edu.ncsu.csc.itrust.exception.ITrustException"%>

<%@include file="/global.jsp"%>

<%
	pageTitle = "iTrust - Appointment Requests";
%>
<%@include file="/header.jsp"%>
<%
	EditApptAction editAction = new EditApptAction(prodDAO, loggedInMID.longValue());
	AddApptRequestAction action = new AddApptRequestAction(prodDAO); //ViewAppt in HCP
	ViewVisitedHCPsAction hcpAction = new ViewVisitedHCPsAction(
			prodDAO, loggedInMID.longValue()); //ViewAppt in HCP
	List<HCPVisitBean> visits = hcpAction.getVisitedHCPs();

	ApptTypeDAO apptTypeDAO = prodDAO.getApptTypeDAO();
	List<ApptTypeBean> apptTypes = apptTypeDAO.getApptTypes();
	String msg = "";
	long hcpid = 0L;
	String comment = "";
	String date = "";
	String hourI = "";
	String minuteI = "";
	String tod = "";
	String apptType = "";
	String prompt = "";

	ViewMyApptsAction viewAction = new ViewMyApptsAction(prodDAO, loggedInMID.longValue());
	ApptBean original = null;
	String aptParameter = "";
	if (request.getParameter("apt") != null) {
		aptParameter = request.getParameter("apt");
		try {
			int apptID = Integer.parseInt(aptParameter);
			original = editAction.getAppt(apptID);
			if (original == null){
				response.sendRedirect("viewMyApptsPatient.jsp");
			}
		} catch (NullPointerException npe) {
			response.sendRedirect("viewMyApptsPatient.jsp");
		} catch (NumberFormatException e) {
			// Handle Exception
			response.sendRedirect("viewMyApptsPatient.jsp");
		}
	} else {
		response.sendRedirect("viewMyApptsPatient.jsp");
	}
	
	Long patientID = 0L;
	if (session.getAttribute("pid") != null) {
		String pidString = (String) session.getAttribute("pid");
		patientID = Long.parseLong(pidString);
		try {
			editAction.getName(patientID);
		} catch (ITrustException ite) {
			patientID = 0L;
		}
	}
	
	boolean hideForm = false;
	Date oldDate = new Date(original.getDate().getTime());
	DateFormat dFormat = new SimpleDateFormat("MM/dd/yyyy");
	DateFormat tFormat = new SimpleDateFormat("hhmma");
	String hPart = tFormat.format(oldDate).substring(0,2);
	String mPart = tFormat.format(oldDate).substring(2,4);
	String aPart = tFormat.format(oldDate).substring(4);
	
	String lastSchedDate=dFormat.format(oldDate);
	String lastApptType=original.getComment();
	String lastTime1=hPart;
	String lastTime2=mPart;
	String lastTime3=aPart;
	String lastComment=original.getComment();
	if(lastComment == null) lastComment="";

	if (request.getParameter("editAppt") != null && request.getParameter("apptID") != null) {
		String headerMessage = "";
		if (request.getParameter("editAppt").equals("Request")) {
			ApptBean appt = new ApptBean();
			appt.setPatient(loggedInMID);
			hcpid = Long.parseLong(request.getParameter("lhcp"));
			appt.setHcp(hcpid);
			comment = request.getParameter("comment");
			appt.setComment(comment);
			SimpleDateFormat frmt = new SimpleDateFormat("MM/dd/yyyy hh:mm a");
			date = request.getParameter("startDate");
			date = date.trim();
			hourI = request.getParameter("time1");
			minuteI = request.getParameter("time2");
			tod = request.getParameter("time3");
			apptType = request.getParameter("apptType");
			appt.setApptType(apptType);
			try {		
				if(date.length() == 10){
					Date d = frmt.parse(date + " " + hourI + ":" + minuteI + " " + tod);
					appt.setDate(new Timestamp(d.getTime()));
					if(appt.getDate().before(new Timestamp(System.currentTimeMillis()))){
						msg = "ERROR: The scheduled date of this appointment has already passed.";
					}
					else {
						ApptRequestBean req = new ApptRequestBean();
						req.setRequestedAppt(appt);
						msg = action.addApptRequest(req);
						if (msg.contains("conflicts")) {
							msg = "ERROR: " + msg;
							frmt = new SimpleDateFormat("MM/dd/yyyy hh:mm a");
							List<ApptBean> open = action.getNextAvailableAppts(3, appt);
							prompt="<br/>The following nearby time slots are available:<br/>";
							int index = 0;
							for(ApptBean possible : open) {
								index++;
								String newDate = frmt.format(possible.getDate());
								prompt += "<div style='padding:5px;margin:5px;float:left;border:1px solid black;'><b>Option ";
								prompt += index+ "</b><br/>"+ frmt.format(possible.getDate()); 
								prompt +="<form action='appointmentRequests.jsp' method='post'>"
									+"<input type='hidden' name='lhcp' value='"+hcpid+"'/>"
									+"<input type='hidden' name='apptType' value='"+apptType+"'/>	"
									+"<input type='hidden' name='startDate' value='"+newDate.substring(0,10)+"'/>"
									+"<input type='hidden' name='time1' value='"+newDate.substring(11,13)+"'/>"
									+"<input type='hidden' name='time2' value='"+newDate.substring(14,16)+"'/>"
									+"<input type='hidden' name='time3' value='"+newDate.substring(17)+"'/>"
									+"<input type='hidden' name='comment' value='"+comment+"'/>"
									+"<input type='submit' name='request' value='Select this time'/>"
									+"</form></div>";
							}
							prompt+="<div style='clear:both;'><br/></div>";
						} else {
							loggingAction.logEvent(
								TransactionType.APPOINTMENT_REQUEST_SUBMITTED,
								loggedInMID, hcpid, "");
						}
					}
				} else {
					msg = "ERROR: Date must by in the format: MM/dd/yyyy";
				}
			} catch (ParseException e) {
				msg = "ERROR: Date must by in the format: MM/dd/yyyy";
			}
		}
	}
%>
<h1>Request a New Appointment</h1>
<%
	if (msg.contains("ERROR")) {
%>
		<span class="iTrustError"><%=msg%></span>
<%
	} else {
%>
		<span class="iTrustMessage"><%=msg%></span>
<%
	}
%>
<%=prompt%>
<%
	Date d = new Date(original.getDate().getTime());
	DateFormat format = new SimpleDateFormat("MM/dd/yyyy hh:mm a");
%>
<form id="mainForm" type="hidden" method="post" action="editApptPatient.jsp?apt=<%=aptParameter %>&apptID=<%=original.getApptID() %>">
	<p>HCP:</p>
	<select name="lhcp">
<%
	for(HCPVisitBean visit : visits){ 
        if (visit.getHCPMID() == original.getHcp()){
%>          
            <option  selected="selected" value="<%=visit.getHCPMID()%>"><%=visit.getHCPName()%></option>  
<%
		} else { 
%> 
        	<option value="<%=visit.getHCPMID()%>"><%=visit.getHCPName()%></option>  
<%
		}
	}
%>
	</select>
	<p>Appointment Type:</p>
	<select name="apptType">
<%
			for (ApptTypeBean appt : apptTypes) {	
        		if (appt.getName().equals(original.getApptType())) {
%>          
            		<option  selected="selected" value="<%=appt.getName()%>"><%=appt.getName()%></option>  
<%
				} else {
%> 
        			<option value="<%=appt.getName()%>"><%=appt.getName()%></option>  
<%
				}
			}	
		String startDate = "";
%>
	</select>
	<p>Date:</p>
	<%DateFormat format_1 = new SimpleDateFormat("MM/dd/yyyy");%>
	<input name="startDate" value="<%=StringEscapeUtils.escapeHtml("" + ( format_1.format(d) )) %>">
	<input type=button value="Select Date" onclick="displayDatePicker('startDate');">

	<p>Time:</p>
	<%DateFormat format_hour = new SimpleDateFormat("hh");%>
	<%DateFormat format_minute = new SimpleDateFormat("mm");%>
	<%DateFormat format_ampm = new SimpleDateFormat("a");%>
	<select name="time1">
<%
			String hour = "";
			for (int i = 1; i <= 12; i++) {
				if (i < 10)
					hour = "0" + i;
				else
					hour = i + "";
				if (hour.equals((format_hour.format(d) ))) {
%>
					<option selected="selected" value="<%=hour%>"><%=StringEscapeUtils.escapeHtml("" + (hour))%></option>
<%
				} else {
%>
					<option value="<%=hour%>"><%=StringEscapeUtils.escapeHtml("" + (hour))%></option>
<%
				}
			}
%>
	</select>:<select name="time2">
<%
			String min = "";
			for (int i = 0; i < 60; i += 5) {
				if (i < 10)
					min = "0" + i;
				else
					min = i + "";
				if (min.equals((format_minute.format(d) ))) {
%>
							<option selected="selected" value="<%=min%>"><%=StringEscapeUtils.escapeHtml("" + (min))%></option>
<%
				} else {
%>
							<option value="<%=min%>"><%=StringEscapeUtils.escapeHtml("" + (min))%></option>
<%
				}
			}
%>
	</select> <select name="time3">
<% 
		if (format_ampm.format(d).equals("AM")){ 
%>
			<option value="PM">PM</option>
			<option selected="selected" value="AM">AM</option>
<%
		} else {
%>
			<option value="AM">AM</option>
			<option selected="selected" value="PM">PM</option>
<% 
		}
%>
	</select>
	<p>Comment:</p>
	<textarea name="comment" cols="100" rows="5"><%=StringEscapeUtils.escapeHtml("" + (comment))%></textarea>
	<br />
	<br /> 
	<input type="hidden" id="editAppt" name="editAppt" value=""/>
	<input type="submit" name="request" value="Request" onClick="document.getElementById('editAppt').value='Request'"/>
</form>

<%@include file="/footer.jsp"%>
