<%@taglib uri="/WEB-INF/tags.tld" prefix="itrust"%>
<%@page errorPage="/auth/exceptionHandler.jsp"%>

<%@page import="edu.ncsu.csc.itrust.dao.mysql.CauseOfDeathTrendsReportDAO"%>
<%@page import="edu.ncsu.csc.itrust.beans.CauseOfDeathTrendsReportBean"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Iterator"%>
<%@page import="edu.ncsu.csc.itrust.exception.FormValidationException"%>


<%@include file="/global.jsp" %>

<%
pageTitle = "iTrust - View Cause-Of-Death Trends Report";

String view = request.getParameter("viewSelect");
%>

<%@include file="/header.jsp" %>
<%
    //log the page view
    loggingAction.logEvent(TransactionType.DEATH_TRENDS_VIEW, loggedInMID.longValue(), 0, "View cause-of-death trends report");

	CauseOfDeathTrendsReportDAO report = new CauseOfDeathTrendsReportDAO(prodDAO);
	List<CauseOfDeathTrendsReportBean> allDeaths = null;

	//get form data

	long mid = Long.valueOf(request.getUserPrincipal().getName());

    String startDate = request.getParameter("startDate");
    String endDate = request.getParameter("endDate");

	String gender = request.getParameter("gender");

    String patients = request.getParameter("patients");
    if (patients == null)
        patients = "";

    //try to get the report. If there's an error, print it. If null is returned, it's the first page load
    if(patients.equalsIgnoreCase("My Patients")) {
        try {
            allDeaths = report.getTopTwoDeathsForHCP(mid, gender, startDate, endDate);
        } catch(FormValidationException e){
            e.printHTML(pageContext.getOut());
        }
     }

    if(patients.equalsIgnoreCase("All Patients") || patients.equalsIgnoreCase("")) {
        try {
            allDeaths = report.getTopTwoDeathsForAll(gender, startDate, endDate);
        } catch(FormValidationException e){
            e.printHTML(pageContext.getOut());
        }
     }

	if (gender == null)
        gender = "";
    if (startDate == null)
    	startDate = "";
    if (endDate == null)
    	endDate = "";

%>
<br />
<form action="causeOfDeathTrendsReport.jsp" method="post" id="formMain">
<input type="hidden" name="viewSelect" />
<table class="fTable" align="center" id="causeOfDeathTrendsReportTable">
	<tr>
		<th colspan="4">Cause of Death Trends Report</th>
	</tr>
	<tr class="subHeader">
    		<td>Patients:</td>
    		<td>
    			<select name="patients" style="font-size:10" >
    			<%
                    if (patients != null && !patients.equalsIgnoreCase("")) {
                     if (patients.equalsIgnoreCase("All Patients")) {
                %>
                		<option selected="selected" value="All Patients"> All Patients </option>
                		<option value="My Patients"> My Patients </option>
                <%
                     } else if (patients.equalsIgnoreCase("My Patients")) {
                %>
                        <option value="All Patients"> All Patients </option>
                        <option selected="selected" value="My Patients"> My Patients </option>
                <%
                        }
                     } else {
                %>
                       <option value="All Patients"> All Patients </option>
                       <option value="My Patients"> My Patients </option>
                </select>
                <%
                    }
                %>
    		</td>
    		<td>Gender:</td>
    		<td>
    			<select name="gender" style="font-size:10" >
    			<%
                    if (gender != null && !gender.equalsIgnoreCase("")) {
                	    if (gender.equalsIgnoreCase("All")) {
                %>
                		<option selected="selected" value="All"> All </option>
                		<option value="Male"> Male </option>
                		<option value="Female"> Female </option>
                	<%
                    	} else if(gender.equalsIgnoreCase("Male")){
                    %>
                        <option value="All"> All </option>
                        <option selected="selected" value="Male"> Male </option>
                        <option value="Female"> Female </option>
                    <%
                        } else if(gender.equalsIgnoreCase("Female")){
                    %>
                        <option value="All"> All </option>
                        <option value="Male"> Male </option>
                        <option selected="selected" value="Female"> Female </option>
                    <%
                            }
                        } else {
                    %>
                        <option value="All"> All </option>
                        <option value="Male"> Male </option>
                        <option value="Female"> Female </option>
                </select>
                <% } %>
    		</td>
    	</tr>
	<tr class="subHeader">
		<td>Start Date:</td>
		<td>
			<input name="startDate" value="<%= StringEscapeUtils.escapeHtml("" + (startDate)) %>" size="10">
            <input type=button value="Select Date" onclick="displayDatePicker('startDate');">
		</td>
		<td>End Date:</td>
		<td>
			<input name="endDate" value="<%= StringEscapeUtils.escapeHtml("" + (endDate)) %>" size="10">
            <input type=button value="Select Date" onclick="displayDatePicker('endDate');">
		</td>
	</tr>
	<tr>
		<td colspan="4" style="text-align: center;"><input type="submit" id="view_report" value="View Report"></td>
	</tr>
</table>

</form>

<br />

<% if (allDeaths != null) { %>

<table class="fTable" align="center" id="causeOfDeathTrendsTable">
<tr>
	<th>ICD-9CM Name</th>
	<th>ICD-9CM Code</th>
	<th>Quantity of Deaths</th>
</tr>

<%
    Iterator<CauseOfDeathTrendsReportBean> iterator = allDeaths.iterator();

    while(iterator.hasNext()) {

    CauseOfDeathTrendsReportBean reports = iterator.next();
   %>
<tr style="text-align:center;">
	<td><%=reports.getCause()%></td>
	<td><%=reports.getCode()%></td>
	<td><%=reports.getDeaths()%></td>
</tr>
    <%
    }
    %>

</table>

<% } %>
<br />

<%@include file="/footer.jsp" %>