<%@taglib uri="/WEB-INF/tags.tld" prefix="itrust"%>
<%@page errorPage="/auth/exceptionHandler.jsp"%>

<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="edu.ncsu.csc.itrust.beans.PatientBean"%>
<%@page import="edu.ncsu.csc.itrust.beans.PatientVisitBean"%>

<%@page import="edu.ncsu.csc.itrust.action.ViewPreRegisteredPatientListAction"%>

<%@include file="/global.jsp" %>

<%
pageTitle = "iTrust - View All Pre-Registered Patients";
%>

<%@include file="/header.jsp" %>

<%
ViewPreRegisteredPatientListAction action = new ViewPreRegisteredPatientListAction(prodDAO, loggedInMID.longValue());

List<PatientBean> PreRegisteredPatientList = action.getPreRegisteredPatients();

loggingAction.logEvent(TransactionType.PATIENT_LIST_VIEW, loggedInMID, 0, "");

%>

<script src="/iTrust/DataTables/media/js/jquery.dataTables.min.js" type="text/javascript"></script>
	<script type="text/javascript">
	jQuery.fn.dataTableExt.oSort['lname-asc']  = function(x,y) {
		var a = x.split(" ");
		var b = y.split(" ");
		return ((a[1] < b[1]) ? -1 : ((a[1] > b[1]) ?  1 : 0));
	};

	jQuery.fn.dataTableExt.oSort['lname-desc']  = function(x,y) {
		var a = x.split(" ");
		var b = y.split(" ");
		return ((a[1] < b[1]) ? 1 : ((a[1] > b[1]) ?  -1 : 0));
	};
</script>

<script type="text/javascript">    
	$(document).ready(function() {
						$("#patientList").dataTable( {
							"aaColumns": [ [2,'dsc'] ],
							"aoColumns": [ { "sType": "lname" }, null, null],
							"bStateSave": true,
							"sPaginationType": "full_numbers"
						});
					});
</script>


<style type="text/css" title="currentStyle">
@import "/iTrust/DataTables/media/css/demo_table.css";        
</style>

<br />
	<h2>Pre-Registered Patients</h2>

<form action="viewReport.jsp" method="post" name="myform">
<table class="display fTable" id="patientList" align="center">

	<thead>
	<tr class="">
		<th>Deactivate</th>
		<th>Name</th>
		<th>Address</th>
		<th>Email</th>

	</tr>
	</thead>
	<tbody>
	<%
		List<PatientBean> patientsList = new ArrayList<PatientBean>();
		int index = 0;

		for (PatientBean bean : PreRegisteredPatientList) {
			patientsList.add(bean);
			String idName = "row" + index;
			String editButtonName = "button" + index;
	%>

	<tr>
		<td>
			<a  href="deactivatePrePatient.jsp?patient=<%= StringEscapeUtils.escapeHtml("" + (index)) %>" style="text-decoration: none;">
				<input type=button value="Deactivate" class="deactivate">
			</a>
		</td>

		<td >
			<a href="viewPreRegisteredPatientInfo.jsp?patient=<%= StringEscapeUtils.escapeHtml("" + (index)) %>" id=<%=editButtonName%>>

			<%= StringEscapeUtils.escapeHtml("" + (bean.getFullName())) %>

			</a>
		</td>
		<td ><%= StringEscapeUtils.escapeHtml("" + (bean.getStreetAddress1() +" " +bean.getStreetAddress2() +" " +bean.getCity() +" " +bean.getState()) +" " +bean.getZip()) %></td>
		<td >
			<%= StringEscapeUtils.escapeHtml("" + (bean.getEmail())) %>
		</td>
	</tr>
	<%
			index ++;
		}

		session.setAttribute("patients", patientsList);
	%>
	</tbody>
</table>
</form>
<script type="text/javascript">
		$('.deactivate').on('click', function() {
			$(this).closest('tr').hide();
		});
</script>

<br />
<br />

<%@include file="/footer.jsp" %>
