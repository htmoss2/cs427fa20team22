package edu.ncsu.csc.itrust.dao.mysql;

import edu.ncsu.csc.itrust.DBUtil;
import edu.ncsu.csc.itrust.beans.CauseOfDeathTrendsReportBean;
import edu.ncsu.csc.itrust.dao.DAOFactory;
import edu.ncsu.csc.itrust.exception.DBException;
import edu.ncsu.csc.itrust.exception.FormValidationException;
import edu.ncsu.csc.itrust.exception.ITrustException;

import java.sql.*;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.List;

public class CauseOfDeathTrendsReportDAO {

	private DAOFactory factory;

	/**
	 * The typical constructor.
	 * @param factory The {@link DAOFactory} associated with this DAO, which is used for obtaining SQL connections, etc.
	 */
	public CauseOfDeathTrendsReportDAO(DAOFactory factory) {
		this.factory = factory;
    }

	/**
	* Returns a list of patients for a particular HCP
	*
	* @param hcpMID The MID of the HCP to look up.
	* @return A java.util.List of PatientsForHCP.
	* @throws DBException
	*/
    public List<Long> getPatientsForHCP(long hcpMID) throws DBException {
		Connection conn = null;
		PreparedStatement stmt = null;
		List<Long> patients = new ArrayList<Long>();

		try {
			conn = factory.getConnection();
			stmt = conn.prepareStatement("SELECT DISTINCT PatientID FROM officevisits WHERE HCPID = ?");
			stmt.setLong(1, hcpMID);
			ResultSet rs = stmt.executeQuery();
			while(rs.next()) {
				Long patient = rs.getLong("PatientID");
				patients.add(patient);
			}
			rs.close();
		} catch (SQLException e) {
			throw new DBException(e);
		} finally {
			DBUtil.closeConnection(conn, stmt);
		}
		return patients;
	}

	/**
	* Returns a list of the top two causes of deaths for a particular HCP
	*
	* @param hcpMID The MID of the HCP to look up.
	* @param gender Gender of the patients - All, Female, Male
	* @param startingDate Start date of search.
	* @param endingDate End date of search.
	* @return A java.util.List of TopTwoDeathsForHCP.
	* @throws DBException
	*/
	public ArrayList<CauseOfDeathTrendsReportBean> getTopTwoDeathsForHCP(long hcpMID, String gender, String startingDate, String endingDate) throws FormValidationException, ITrustException {
		Connection conn = null;
		PreparedStatement stmt = null;
		List<Long> patients = this.getPatientsForHCP(hcpMID);
		ArrayList<CauseOfDeathTrendsReportBean> causes;
		String query = null;
		java.sql.Date startDate;
		java.sql.Date endDate;

		try {

			if (startingDate == null || endingDate == null)
				return null;

			causes = new ArrayList<>();

			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy");
			DateTimeFormatter formatter2 = DateTimeFormatter.ofPattern("yyyy-MM-dd");
			String lower = LocalDate.parse(startingDate, formatter).format(formatter2);
			String upper = LocalDate.parse(endingDate, formatter).format(formatter2);

			startDate = Date.valueOf(lower);
			endDate = Date.valueOf(upper);

			if (startDate.after(endDate))
				throw new FormValidationException("Start date must be before end date!");

			conn = factory.getConnection();

			if(gender.equalsIgnoreCase("All")){
				query = "SELECT DISTINCT CauseOfDeath, COUNT(CauseOfDeath) FROM patients WHERE MID IN (";
				for(int i = 0; i < patients.size(); i++)
				{
					if (i+1 == patients.size()){
						query += patients.get(i);
					}
					else{
						query += patients.get(i) + ",";
					}

				}
				query += ") AND DateOfDeath IS NOT NULL AND YEAR(DateOfDeath) >= YEAR(?) AND YEAR(DateOfDeath) <= YEAR(?)"
				+ "GROUP BY CauseOfDeath ORDER BY COUNT(CauseOfDeath) DESC";
				stmt = conn.prepareStatement(query);
				stmt.setDate(1, startDate);
				stmt.setDate(2, endDate);
			}

			else{
				query = "SELECT DISTINCT CauseOfDeath, COUNT(CauseOfDeath) FROM patients WHERE MID IN (";
				for(int i = 0; i < patients.size(); i++)
				{
					if (i+1 == patients.size()){
						query += patients.get(i);
					}
					else{
						query += patients.get(i) + ",";
					}
				}
				query += ") AND DateOfDeath IS NOT NULL AND Gender = ? AND YEAR(DateOfDeath) >= YEAR(?) AND YEAR(DateOfDeath) <= YEAR(?)"
				+ "GROUP BY CauseOfDeath ORDER BY COUNT(CauseOfDeath) DESC";
				stmt = conn.prepareStatement(query);
				stmt.setString(1, gender);
				stmt.setDate(2, startDate);
				stmt.setDate(3, endDate);
			}
			ResultSet rs = stmt.executeQuery();
			int count = 0;
			while(rs.next() && count < 2) {
				String cause = rs.getString("CauseOfDeath");
				String deaths = rs.getString("COUNT(CauseOfDeath)");
				String name = this.getCodeName(cause);
				causes.add(new CauseOfDeathTrendsReportBean(name, cause, deaths));
				count++;
			}

			if (causes.isEmpty())
				throw new FormValidationException("No results returned");

			rs.close();
			stmt.close();
		} catch (SQLException e) {
			throw new DBException(e);
		} catch (DateTimeParseException e) {
			throw new FormValidationException("Enter dates in MM/dd/yyyy");
		} finally {
			DBUtil.closeConnection(conn, stmt);
		}
		return causes;
	}

	/**
	* Returns a list of the top two causes of deaths for a all HCPs
	*
	* @param gender Gender of the patients - All, Female, Male
	* @param startingDate Start date of search.
	* @param endingDate End date of search.
	* @return A java.util.List of TopTwoDeathsForAll.
	* @throws DBException
	*/
    public ArrayList<CauseOfDeathTrendsReportBean> getTopTwoDeathsForAll(String gender, String startingDate, String endingDate) throws FormValidationException, ITrustException {
    	Connection conn = null;
		PreparedStatement stmt = null;
		ArrayList<CauseOfDeathTrendsReportBean> causes;
		String query = null;
		java.sql.Date startDate;
		java.sql.Date endDate;

		try {

			if (startingDate == null || endingDate == null)
				return null;

			causes = new ArrayList<>();

			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy");
			DateTimeFormatter formatter2 = DateTimeFormatter.ofPattern("yyyy-MM-dd");
			String lower = LocalDate.parse(startingDate, formatter).format(formatter2);
			String upper = LocalDate.parse(endingDate, formatter).format(formatter2);

			startDate = Date.valueOf(lower);
			endDate = Date.valueOf(upper);

			if (startDate.after(endDate))
				throw new FormValidationException("Start date must be before end date!");

			conn = factory.getConnection();

			if(gender.equalsIgnoreCase("All")){
				query = "SELECT DISTINCT CauseOfDeath, COUNT(CauseOfDeath) FROM patients"
				+ " WHERE DateOfDeath IS NOT NULL AND YEAR(DateOfDeath) >= YEAR(?) AND YEAR(DateOfDeath) <= YEAR(?)"
				+ "GROUP BY CauseOfDeath ORDER BY COUNT(CauseOfDeath) DESC";
				stmt = conn.prepareStatement(query);
				stmt.setDate(1, startDate);
				stmt.setDate(2, endDate);
			}

			else{
				query = "SELECT DISTINCT CauseOfDeath, COUNT(CauseOfDeath) FROM patients"
				+ " WHERE DateOfDeath IS NOT NULL AND Gender = ? AND YEAR(DateOfDeath) >= YEAR(?) AND YEAR(DateOfDeath) <= YEAR(?)"
				+ "GROUP BY CauseOfDeath ORDER BY COUNT(CauseOfDeath) DESC";
				stmt = conn.prepareStatement(query);
				stmt.setString(1, gender);
				stmt.setDate(2, startDate);
				stmt.setDate(3, endDate);
			}
			ResultSet rs = stmt.executeQuery();
			int count = 0;
			while(rs.next() && count < 2) {
				String cause = rs.getString("CauseOfDeath");
				String deaths = rs.getString("COUNT(CauseOfDeath)");
				String name = this.getCodeName(cause);
				causes.add(new CauseOfDeathTrendsReportBean(name, cause, deaths));
				count++;
			}

			if (causes.isEmpty())
				throw new FormValidationException("No results returned");

			rs.close();
			stmt.close();
		} catch (SQLException e) {
			throw new DBException(e);
		} catch (DateTimeParseException e) {
			throw new FormValidationException("Enter dates in MM/dd/yyyy");
		} finally {
			DBUtil.closeConnection(conn, stmt);
		}
		return causes;
	}

	/**
	* Returns the code name for a particular ICD-9CM code.
	* 
	* @param code ICD-9CM code.
	* @return the name of the ICD-9CM code.
	* @throws DBException
	*/
    public String getCodeName(String code) throws DBException {
		Connection conn = null;
		PreparedStatement stmt = null;
		String result = null;

		try {
			conn = factory.getConnection();
			stmt = conn.prepareStatement("SELECT Description FROM icdcodes WHERE Code = ?");
			stmt.setString(1, code);
			ResultSet rs = stmt.executeQuery();
			if(rs.next()){
				result = rs.getString("Description");
				rs.close();
				stmt.close();
			}
			else{
				rs.close();
				stmt.close();
				return null;
			}
		} catch (SQLException e) {
			throw new DBException(e);
		} finally {
			DBUtil.closeConnection(conn, stmt);
		}
		return result;
	}
}
