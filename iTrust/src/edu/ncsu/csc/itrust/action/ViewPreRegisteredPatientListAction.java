package edu.ncsu.csc.itrust.action;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import edu.ncsu.csc.itrust.beans.OfficeVisitBean;
import edu.ncsu.csc.itrust.beans.PatientBean;
import edu.ncsu.csc.itrust.beans.PatientVisitBean;
import edu.ncsu.csc.itrust.beans.PersonnelBean;
import edu.ncsu.csc.itrust.dao.DAOFactory;
import edu.ncsu.csc.itrust.dao.mysql.OfficeVisitDAO;
import edu.ncsu.csc.itrust.dao.mysql.PatientDAO;
import edu.ncsu.csc.itrust.dao.mysql.PersonnelDAO;
import edu.ncsu.csc.itrust.exception.DBException;
import edu.ncsu.csc.itrust.exception.ITrustException;

/**
 *
 * Action class for viewPreRegisteredPatientList.jsp
 *
 */
public class ViewPreRegisteredPatientListAction {
	private long loggedInMID;
	private PatientDAO patientDAO;
	private PersonnelDAO personnelDAO;

	private ArrayList<PatientBean> PreRegisteredPatients;

	/**
	 * Set up defaults
	 * @param factory The DAOFactory used to create the DAOs used in this action.
	 * @param loggedInMID The MID of the person viewing the office visits.
	 */
	public ViewPreRegisteredPatientListAction(DAOFactory factory, long loggedInMID) {
		this.loggedInMID = loggedInMID;

		this.personnelDAO = factory.getPersonnelDAO();

		this.patientDAO = factory.getPatientDAO();

		PreRegisteredPatients = new ArrayList<PatientBean>();
	}

	/**
	 * Identify all the PreRegistered Patients from Patients Table using  Users Table Role="PreRegistered", and add them to a list.
	 *
	 * @throws ITrustException
	 */
	public List<PatientBean> getPreRegisteredPatients() throws ITrustException {
		try {

			List<PatientBean> plist = patientDAO.getAllPreRegisteredPatients();

			for(PatientBean pb : plist) {

				PreRegisteredPatients.add(pb);
			}

			return PreRegisteredPatients;
		}
		catch (DBException dbe) {
			throw new ITrustException(dbe.getMessage());
		}
	}
}