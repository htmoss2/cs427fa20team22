package edu.ncsu.csc.itrust.action;

import edu.ncsu.csc.itrust.beans.PatientBean;

import java.io.PrintWriter;
import java.io.StringWriter;

import edu.ncsu.csc.itrust.beans.HealthRecord;
import edu.ncsu.csc.itrust.beans.forms.HealthRecordForm;
import edu.ncsu.csc.itrust.dao.DAOFactory;
import edu.ncsu.csc.itrust.dao.mysql.PatientDAO;
import edu.ncsu.csc.itrust.dao.mysql.AuthDAO;
import edu.ncsu.csc.itrust.dao.mysql.HealthRecordsDAO;
import edu.ncsu.csc.itrust.enums.Role;
import edu.ncsu.csc.itrust.exception.DBException;
import edu.ncsu.csc.itrust.exception.FormValidationException;
import edu.ncsu.csc.itrust.exception.ITrustException;
import edu.ncsu.csc.itrust.validate.AddPatientValidator;
import edu.ncsu.csc.itrust.validate.HealthRecordFormValidator;

/**
 * Used for Add Pre-registered Patient page (PreRegisterPatient.jsp). This just adds an empty patient, creates an entered password for
 * that patient.
 * 
 * Very similar to {@link AddOfficeVisitAction}
 * 
 * 
 */
public class AddPreRegisteredPatientAction {
	private PatientDAO patientDAO;
	private AuthDAO authDAO;
    private long loggedInMID;
    private HealthRecordsDAO healthDAO;

	/**
	 * Just the factory and logged in MID
	 * 
	 * @param factory
	 * @param loggedInMID
	 */
	public AddPreRegisteredPatientAction(DAOFactory factory, long loggedInMID) {
		this.patientDAO = factory.getPatientDAO();
		this.loggedInMID = loggedInMID;
        this.authDAO = factory.getAuthDAO();
        this.healthDAO = factory.getHealthRecordsDAO();
	}
	
	/**
	 * Creates a new patient, returns the new MID. Adds a new user to the table with a 
	 * preRegisteredPatient role
	 * 
	 * @param p patient to be created
	 * @return the new MID of the patient
	 * @throws FormValidationException if the patient is not successfully validated
	 * @throws ITrustException 
	 */

	
	public long addPatient(PatientBean p, HealthRecordForm h) throws FormValidationException, ITrustException {
        new AddPatientValidator().validate(p);
        new HealthRecordFormValidator().validatePreregistration(h);
        
        // Make sure the email is unique
        if (patientDAO.isEmailInUse(p.getEmail())) {
            throw new ITrustException("This email is already in use. Please use another email address.");
        }
		
		long newMID = patientDAO.addEmptyPatient();   // the new added row id in the database
		p.setMID(newMID);
		
		String pwd = authDAO.addUser(newMID, Role.PREREGISTEREDPATIENT, p.getPassword()); 
		
		p.setPassword(pwd);
        patientDAO.editPatient(p, loggedInMID);

        // Convert the HealthRecordForm into a HeathRecord
        HealthRecord r = formToRecord(h, newMID);
        healthDAO.add(r);

        return newMID;
    }
    
    // TODO: this is taken directly from EditHealthAction.java (DRY)
    private HealthRecord formToRecord(HealthRecordForm form, long pid) {
        HealthRecord record = new HealthRecord();
        record.setPatientID(pid);
        if (!"".equals(form.getHeight()))
            record.setHeight(Double.parseDouble(form.getHeight()));
        if (!"".equals(form.getWeight()))
		    record.setWeight(Double.parseDouble(form.getWeight()));
		record.setSmoker(Integer.parseInt(form.getIsSmoker()));
		return record;
    }
}
