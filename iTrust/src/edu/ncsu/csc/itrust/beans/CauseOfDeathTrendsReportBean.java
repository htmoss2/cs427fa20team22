package edu.ncsu.csc.itrust.beans;


public class CauseOfDeathTrendsReportBean {

    /** The name of the diagnosis cause of death */
    String cause;
    /** The code of the diagnosis cause of death */
    String code;
    /** The quantity of the deaths */
    String deaths;

    /**
     * Constructor for an empty bean
     */
    public CauseOfDeathTrendsReportBean() {
    }

    /**
     * Constructor for the bean. Accepts cause of death name, cause of death code, and quantity of deaths
     * @param cause The cause of death name
     * @param code The cause of death code
     * @param deaths The quantity of deaths for the chosen death code
     */
    public CauseOfDeathTrendsReportBean(String cause, String code, String deaths) {
        this.cause = cause;
        this.code = code;
        this.deaths = deaths;
    }

    /**
     * Getter for cause of death name
     * @return The stored cause of death name
     */
    public String getCause() {
        return cause;
    }

    /**
     * Setter for cause of death name
     * @param cause The cause of death name to be stored
     */
    public void setCause(String cause) {
        this.cause = cause;
    }

    /**
     * Getter for cause of death code
     * @return The stored cause of death code
     */
    public String getCode() {
        return code;
    }

    /**
     * Setter for cause of death name
     * @param code The cause of death name to be stored
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * Getter for quantity of deaths
     * @return The stored quantity of deaths
     */
    public String getDeaths() {
        return deaths;
    }

    /**
     * Setter for quantity of deaths
     * @param deaths The stored quantity of deaths
     */
    public void setDeaths(String deaths) {
        this.deaths = deaths;
    }

}
