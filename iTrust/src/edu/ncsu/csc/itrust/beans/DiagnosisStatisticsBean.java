package edu.ncsu.csc.itrust.beans;

import java.util.Date;

/**
 * A bean for storing data about diagnosis counts in an area for a date range.
 * 
 * A bean's purpose is to store data. Period. Little or no functionality is to be added to a bean 
 * (with the exception of minor formatting such as concatenating phone numbers together). 
 * A bean must only have Getters and Setters (Eclipse Hint: Use Source > Generate Getters and 
 * Setters to create these easily)
 * 
 *  
 */
public class DiagnosisStatisticsBean {
	String zipCode;

	long zipStats;
	long regionStats;
	long stateStats;
	long allStats;

	Date startDate;
	Date endDate;
	
	/**
	 * Constructor for an empty bean 
	 */
	public DiagnosisStatisticsBean() {
	}
	
	/**
	 * Constructor for the bean. Accepts region and zip stats and zip code
	 * @param zipCode The zip code analyzed
	 * @param zipStats The count of diagnoses for the zip code
	 * @param regionStats The count of diagnoses for the region
	 */
	public DiagnosisStatisticsBean(String zipCode, long zipStats, long regionStats) {
		this(zipCode, zipStats, regionStats, 0, 0, null, null);
	}

	/**
	 * Constructor for the bean. Accepts region and zip stats, zip code and start date
	 * @param zipCode The zip code analyzed
	 * @param zipStats The count of diagnoses for the zip code
	 * @param regionStats The count of diagnoses for the region
	 * @param startDate The beginning of the data's time period
	 */
	public DiagnosisStatisticsBean(String zipCode, long zipStats, long regionStats,
			Date startDate) {
		this(zipCode, zipStats, regionStats, 0, 0, startDate, null);
	}
	
	/**
	 * Constructor for the bean. Accepts region and zip stats, zip code and date range
	 * @param zipCode The zip code analyzed
	 * @param zipStats The count of diagnoses for the zip code
	 * @param regionStats The count of diagnoses for the region
	 * @param startDate The beginning of the data's time period
	 * @param endDate The end of the data's time period
	 */
	public DiagnosisStatisticsBean(String zipCode, long zipStats, long regionStats, Date startDate,
			Date endDate) {
		this(zipCode, zipStats, regionStats, 0, 0, startDate, endDate);
	}
	
	/**
	 * Constructor for the bean. Accepts all stats and zip code
	 * @param zipCode The zip code analyzed
	 * @param zipStats The count of diagnoses for the zip code
	 * @param regionStats The count of diagnoses for the region
	 * @param stateStats The count of diagnoses for the state
	 */
	public DiagnosisStatisticsBean(String zipCode, long zipStats, long regionStats, long stateStats, 
			long allStats) {
		this(zipCode, zipStats, regionStats, stateStats, allStats, null, null);
	}

	/**
	 * Constructor for the bean. Accepts region and zip stats, zip code and date range
	 * @param zipCode The zip code analyzed
	 * @param zipStats The count of diagnoses for the zip code
	 * @param regionStats The count of diagnoses for the region
	 * @param stateStats The count of diagnoses for the state
	 * @param startDate The beginning of the data's time period
	 * @param endDate The end of the data's time period
	 */
	public DiagnosisStatisticsBean(String zipCode, long zipStats, long regionStats, long stateStats,
			long allStats, Date startDate, Date endDate) {
		this.zipCode = zipCode;
		this.zipStats = zipStats;
		this.regionStats = regionStats;
		this.stateStats = stateStats;
        this.allStats = allStats;
        if (startDate != null) {
            this.startDate = (Date) startDate.clone();
        }
        if (endDate != null) {
            this.endDate = (Date) endDate.clone();
        }
	}

	/**
	 * Getter for Zip code
	 * @return The stored Zip code
	 */
	public String getZipCode() {
		return zipCode;
	}

	/**
	 * Setter for Zip code
	 * @param zipCode The zip code to be stored
	 */
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	/**
	 * Getter for Zip code count
	 * @return The count of diagnoses for the zip code
	 */
	public long getZipStats() {
		return zipStats;
	}

	/**
	 * Setter for Zip code count
	 * @param zipStats The count of diagnoses to be stored
	 */
	public void setZipStats(long zipStats) {
		this.zipStats = zipStats;
	}

	/**
	 * Getter for Region count
	 * @return The count of diagnoses for the region
	 */
	public long getRegionStats() {
		return regionStats;
	}

	/**
	 * Setter for Region count
	 * @param regionStats The count of regional diagnoses to be stored
	 */
	public void setRegionStats(long regionStats) {
		this.regionStats = regionStats;
	}
	
	/**
	 * Getter for State count
	 * @return The count of diagnoses for the region
	 */
	public long getStateStats() {
		return stateStats;
	}

	/**
	 * Setter for State count
	 * @param stateStats The count of regional diagnoses to be stored
	 */
	public void setStateStats(long stateStats) {
		this.stateStats = stateStats;
	}
	
	/**
	 * Getter for State count
	 * @return The count of diagnoses for the region
	 */
	public long getAllStats() {
		return allStats;
	}

	/**
	 * Setter for State count
	 * @param stateStats The count of regional diagnoses to be stored
	 */
	public void setAllStats(long allStats) {
		this.allStats = allStats;
	}
	
	/**
	 * Getter for Start Date
	 * @return The start date for the data
	 */
	public Date getStartDate() {
		return (Date) startDate.clone();
	}
	
	/**
	 * Setter for Start Date
	 * @param startDate The data's start date
	 */
	public void setStartDate(Date startDate) {
		this.startDate = (Date) startDate.clone();
	}
	
	/**
	 * Getter for End Date
	 * @return The end date for the data
	 */
	public Date getEndDate() {
		return (Date) endDate.clone();
	}
	
	/**
	 * Setter for the End Date
	 * @param endDate
	 */
	public void setEndDate(Date endDate) {
		this.endDate = (Date) endDate.clone();
	}
}
