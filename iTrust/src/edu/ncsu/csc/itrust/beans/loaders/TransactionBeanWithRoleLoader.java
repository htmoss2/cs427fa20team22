package edu.ncsu.csc.itrust.beans.loaders;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import edu.ncsu.csc.itrust.beans.TransactionWithRoleBean;
import edu.ncsu.csc.itrust.enums.TransactionType;

/**
 * A loader for TransactionBeans.
 * 
 * Loads in information to/from beans using ResultSets and PreparedStatements.
 * Use the superclass to enforce consistency. For details on the paradigm for a
 * loader (and what its methods do), see {@link BeanLoader}
 */
public class TransactionBeanWithRoleLoader implements BeanLoader<TransactionWithRoleBean> {

    public List<TransactionWithRoleBean> loadList(ResultSet rs) throws SQLException {
        List<TransactionWithRoleBean> list = new ArrayList<TransactionWithRoleBean>();
        while (rs.next()) {
            list.add(loadSingle(rs));
        }
        return list;
    }

    public PreparedStatement loadParameters(PreparedStatement ps, TransactionWithRoleBean bean) throws SQLException {
        throw new IllegalStateException("unimplemented!");
    }

    public TransactionWithRoleBean loadSingle(ResultSet rs) throws SQLException {
        TransactionWithRoleBean t = new TransactionWithRoleBean();
        t.setMainRole(rs.getString("mainRole"));
        t.setSecondaryRole(rs.getString("secondaryRole"));
        t.setAddedInfo(rs.getString("addedInfo"));
        t.setLoggedInMID(rs.getLong("loggedInMID"));
        t.setSecondaryMID(rs.getLong("secondaryMID"));
        t.setTimeLogged(rs.getTimestamp("timeLogged"));
        t.setTransactionType(TransactionType.parse(rs.getInt("transactionCode")));
        t.setTransactionID(rs.getLong("transactionID"));
        t.setConvertedDate(rs.getString("convertedDate"));
        return t;
    }

}
