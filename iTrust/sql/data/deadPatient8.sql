DELETE FROM users WHERE MID = 88;
DELETE FROM officevisits WHERE PatientID = 88;
DELETE FROM patients WHERE MID = 88;

INSERT INTO users(MID, password, role, sQuestion, sAnswer) 
	VALUES (88, '30c952fab122c3f9759f02a6d95c3758b246b4fee239957b2d4fee46e26170c4', 'patient', 'what is your favorite color?', 'blue')
    ON DUPLICATE KEY UPDATE MID = MID;

INSERT INTO patients (MID, Gender, DateOfDeath, CauseOfDeath) 
    VALUES (88,'Female','2020-07-01',487.00)
    ON DUPLICATE KEY UPDATE MID = MID;

INSERT INTO officevisits(id,visitDate,HCPID,notes,HospitalID,PatientID)
    VALUES (20778,'2020-07-01',9000000003,'died','1',88)
    ON DUPLICATE KEY UPDATE id = id;