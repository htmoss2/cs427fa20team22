DELETE FROM users WHERE MID = 89;
DELETE FROM officevisits WHERE PatientID = 89;
DELETE FROM patients WHERE MID = 89;

INSERT INTO users(MID, password, role, sQuestion, sAnswer) 
	VALUES (89, '30c952fab122c3f9759f02a6d95c3758b246b4fee239957b2d4fee46e26170c4', 'patient', 'what is your favorite color?', 'blue')
    ON DUPLICATE KEY UPDATE MID = MID;

INSERT INTO patients (MID, Gender, DateOfDeath, CauseOfDeath) 
    VALUES (89,'Male','2020-01-01',487.00)
    ON DUPLICATE KEY UPDATE MID = MID;

INSERT INTO officevisits(id,visitDate,HCPID,notes,HospitalID,PatientID)
    VALUES (20779,'2020-01-01',9000000003,'died','1',89)
    ON DUPLICATE KEY UPDATE id = id;