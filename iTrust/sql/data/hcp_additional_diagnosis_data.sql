INSERT INTO officevisits(ID, visitDate, HCPID, notes, PatientID, HospitalID)
VALUES
(125, '2020-11-18', 9000000000, 'Diagnose Strep', 1, '1'),   
(126, '2020-11-18', 9000000000, 'Diagnose Strep', 2, '1'),   
(127, '2020-11-18', 9000000000, 'Diagnose Strep', 3, '1'),   
(128, '2020-11-18', 9000000000, 'Diagnose Strep', 4, '1'),   
(129, '2020-11-18', 9000000000, 'Diagnose Strep', 21, '1'),  
(130, '2020-11-18', 9000000000, 'Diagnose Strep', 22, '1'),  
(131, '2020-11-18', 9000000000, 'Diagnose Strep', 23, '1'),  
(132, '2020-11-18', 9000000000, 'Diagnose Strep', 24, '1'),  
(133, '2020-11-18', 9000000000, 'Diagnose Strep', 105, '1'), 
(134, '2020-11-18', 9000000000, 'Diagnose Strep', 107, '1'),
(135, '2020-11-18', 9000000000, 'Diagnose Strep', 102, '1')

ON DUPLICATE KEY UPDATE id = id;

INSERT INTO ovdiagnosis(ID, VisitID, ICDCode)
VALUES
(125, 125, 34.00),
(126, 126, 34.00),
(127, 127, 34.00),
(128, 128, 34.00),
(129, 129, 34.00),
(130, 130, 34.00),
(131, 131, 34.00),
(132, 132, 34.00),
(133, 133, 34.00),
(134, 134, 34.00),
(135, 135, 34.00)

ON DUPLICATE KEY UPDATE VisitID = VALUES(VisitID), ICDCode = VALUES(ICDCode);
