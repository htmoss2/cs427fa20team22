DELETE FROM users WHERE MID = 90;
DELETE FROM officevisits WHERE PatientID = 90;
DELETE FROM patients WHERE MID = 90;

INSERT INTO users(MID, password, role, sQuestion, sAnswer) 
	VALUES (90, '30c952fab122c3f9759f02a6d95c3758b246b4fee239957b2d4fee46e26170c4', 'patient', 'what is your favorite color?', 'blue')
    ON DUPLICATE KEY UPDATE MID = MID;

INSERT INTO patients (MID, Gender, DateOfDeath, CauseOfDeath) 
    VALUES (90,'Male','2019-01-01',487.00)
    ON DUPLICATE KEY UPDATE MID = MID;

INSERT INTO officevisits(id,visitDate,HCPID,notes,HospitalID,PatientID)
    VALUES (20780,'2019-01-01',9000000003,'died','1',90)
    ON DUPLICATE KEY UPDATE id = id;