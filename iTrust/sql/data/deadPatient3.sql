DELETE FROM users WHERE MID = 83;
DELETE FROM officevisits WHERE PatientID = 83;
DELETE FROM patients WHERE MID = 83;

INSERT INTO users(MID, password, role, sQuestion, sAnswer) 
	VALUES (83, '30c952fab122c3f9759f02a6d95c3758b246b4fee239957b2d4fee46e26170c4', 'patient', 'what is your favorite color?', 'blue')
    ON DUPLICATE KEY UPDATE MID = MID;

INSERT INTO patients (MID, Gender, DateOfDeath, CauseOfDeath) 
    VALUES (83,'Female','2018-07-01',11.40)
    ON DUPLICATE KEY UPDATE MID = MID;

INSERT INTO officevisits(id,visitDate,HCPID,notes,HospitalID,PatientID)
    VALUES (20773,'2018-07-01',9000000000,'died','1',83)
    ON DUPLICATE KEY UPDATE id = id;