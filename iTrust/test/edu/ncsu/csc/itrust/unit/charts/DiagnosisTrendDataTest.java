package edu.ncsu.csc.itrust.unit.charts;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import org.jfree.data.category.DefaultCategoryDataset;

import de.laures.cewolf.DatasetProduceException;
import edu.ncsu.csc.itrust.beans.DiagnosisStatisticsBean;
import edu.ncsu.csc.itrust.charts.DiagnosisTrendData;
import edu.ncsu.csc.itrust.unit.datagenerators.TestDataGenerator;
import junit.framework.TestCase;

public class DiagnosisTrendDataTest extends TestCase {
	
	private DiagnosisTrendData chart;
	TestDataGenerator gen = new TestDataGenerator();
	
	@Override
	protected void setUp() throws Exception {
		gen.clearAllTables();
		gen.standardData();
		gen.influenza_epidemic();
		gen.malaria_epidemic1();
	}
	
	public void testProduceDataset() {
		
		chart = new DiagnosisTrendData();
		
		String diagnosisName = "Influenza";
		
		Calendar cal = Calendar.getInstance();
		Calendar cal2 = Calendar.getInstance();
		
		cal.set(2011, 10, 31); //start week
		cal2.set(2011, 11, 06); //end week
    	
		DiagnosisStatisticsBean dsBean = new DiagnosisStatisticsBean();
		DiagnosisStatisticsBean avgBean = new DiagnosisStatisticsBean();
		
		dsBean.setZipStats(700);
		dsBean.setRegionStats(700);
		dsBean.setStartDate(cal.getTime());
		dsBean.setEndDate(cal2.getTime());
		
		avgBean.setZipStats(700);
		avgBean.setRegionStats(700);
		dsBean.setStartDate(cal.getTime());
		dsBean.setEndDate(cal.getTime());
		
		Map<String, String> params = new HashMap<String, String>();
		
		try {
			
			chart.initializeAvgDiagnosisStatistics(avgBean, dsBean, diagnosisName);
			DefaultCategoryDataset data = (DefaultCategoryDataset)chart.produceDataset(params);
			assertEquals(700.0, data.getValue(diagnosisName, "Current Week Zipcode Cases"));
			assertEquals(700.0, data.getValue(diagnosisName, "Average Prior Zipcode Cases"));
			assertEquals(700.0, data.getValue(diagnosisName, "Current Week Region Cases"));
			assertEquals(700.0, data.getValue(diagnosisName, "Average Prior Region Cases"));
			assertTrue(chart.hasData());
			
		} catch (DatasetProduceException e) {
			
			
			fail();
			
		}
		
	}
	
	public void testProduceDataset2() {
		
		chart = new DiagnosisTrendData();
		String diagnosisName = "Malaria";
		
		Calendar cal = Calendar.getInstance();
		Calendar cal2 = Calendar.getInstance();
		
		cal.set(2011, 10, 31); //start week
		cal2.set(2011, 11, 06); //end week
    	
		DiagnosisStatisticsBean dsBean = new DiagnosisStatisticsBean();
		DiagnosisStatisticsBean avgBean = new DiagnosisStatisticsBean();
		
		dsBean.setZipStats(700);
		dsBean.setRegionStats(700);
		dsBean.setStartDate(cal.getTime());
		dsBean.setEndDate(cal2.getTime());
		
		avgBean.setZipStats(700);
		avgBean.setRegionStats(700);
		dsBean.setStartDate(cal.getTime());
		dsBean.setEndDate(cal.getTime());
		
		Map<String, String> params = new HashMap<String, String>();
		
		try {
			
			chart.initializeAvgDiagnosisStatistics(avgBean, dsBean, diagnosisName);
			DefaultCategoryDataset data = (DefaultCategoryDataset)chart.produceDataset(params);
			assertEquals(700.0, data.getValue(diagnosisName, "Current Week Zipcode Cases"));
			assertEquals(700.0, data.getValue(diagnosisName, "Average Prior Zipcode Cases"));
			assertEquals(700.0, data.getValue(diagnosisName, "Current Week Region Cases"));
			assertEquals(700.0, data.getValue(diagnosisName, "Average Prior Region Cases"));
			assertTrue(chart.hasData());
			
		} catch (DatasetProduceException e) {
			
			
			fail();
			
		}
	
	}
	
	public void testProduceDataset3() {
		
		chart = new DiagnosisTrendData();
		String diagnosisName = "Mumps";
		
		Calendar cal = Calendar.getInstance();
		Calendar cal2 = Calendar.getInstance();
		
		cal.set(2011, 10, 31); //start week
		cal2.set(2011, 11, 06); //end week
    	
		DiagnosisStatisticsBean dsBean = new DiagnosisStatisticsBean();
		
		dsBean.setZipStats(500);
		dsBean.setRegionStats(500);
		dsBean.setStartDate(cal.getTime());
		dsBean.setEndDate(cal2.getTime());
		
		Map<String, String> params = new HashMap<String, String>();
		
		try {
			
			chart.initializeDiagnosisStatistics(dsBean, diagnosisName);
			DefaultCategoryDataset data = (DefaultCategoryDataset)chart.produceDataset(params);
			assertEquals(500.0, data.getValue(diagnosisName, "Zipcode Cases"));
			assertEquals(500.0, data.getValue(diagnosisName, "Region Cases"));
			assertTrue(chart.hasData());
			
		} catch (DatasetProduceException e) {
			
			
			fail();
			
		}
	
	}

	public void testProduceDataset4() throws ParseException{

		chart = new DiagnosisTrendData();
		String diagnosisName = "Influenza";
		List<DiagnosisStatisticsBean> dsBeans = new ArrayList<DiagnosisStatisticsBean>();


		Date startDate = new SimpleDateFormat("MM/dd/yyyy").parse("11/09/2020");
		Date endDate = new SimpleDateFormat("MM/dd/yyyy").parse("11/15/2020");



		Calendar cal = Calendar.getInstance();
		Calendar cal2 = Calendar.getInstance();

		cal.setTime(startDate); //start week
		cal2.setTime(endDate); //end week

		DiagnosisStatisticsBean dsBean1 = new DiagnosisStatisticsBean();
		DiagnosisStatisticsBean dsBean2 = new DiagnosisStatisticsBean();

		dsBean1.setRegionStats(500);
		dsBean1.setStateStats(560);
		dsBean1.setAllStats(1000);
		dsBean1.setStartDate(cal.getTime());
		dsBean1.setEndDate(cal2.getTime());

		startDate = new SimpleDateFormat("MM/dd/yyyy").parse("11/16/2020");
		endDate = new SimpleDateFormat("MM/dd/yyyy").parse("11/22/2020");

		cal.setTime(startDate); //start week
		cal2.setTime(endDate); //end week

		dsBean2.setRegionStats(550);
		dsBean2.setStateStats(600);
		dsBean2.setAllStats(1000);
		dsBean2.setStartDate(cal.getTime());
		dsBean2.setEndDate(cal2.getTime());

		dsBeans.add(dsBean1);
		dsBeans.add(dsBean2);

		Map<String, String> params = new HashMap<String, String>();

		try {

			chart.initializeDiagnosisStatistics(dsBeans, diagnosisName);
			DefaultCategoryDataset data = (DefaultCategoryDataset)chart.produceDataset(params);
			List keys = data.getColumnKeys();
			assertEquals(500.0, data.getValue("Region", "11/09"));
			assertEquals(560.0, data.getValue("State", "11/09"));
			assertEquals(1000.0, data.getValue("All", "11/09"));
			assertEquals(550.0, data.getValue("Region", "11/16"));
			assertEquals(600.0, data.getValue("State", "11/16"));
			assertEquals(1000.0, data.getValue("All", "11/16"));
			assertTrue(chart.hasData());

		} catch (DatasetProduceException e) {


			fail();

		}

	}
	
}
