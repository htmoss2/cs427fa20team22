/**
 * Tests for AddPatientAction
 */

package edu.ncsu.csc.itrust.unit.action;

import junit.framework.TestCase;
import edu.ncsu.csc.itrust.action.AddPreRegisteredPatientAction;
import edu.ncsu.csc.itrust.beans.PatientBean;
import edu.ncsu.csc.itrust.beans.HealthRecord;
import edu.ncsu.csc.itrust.beans.forms.HealthRecordForm;
import edu.ncsu.csc.itrust.dao.DAOFactory;
import edu.ncsu.csc.itrust.dao.mysql.AuthDAO;
import edu.ncsu.csc.itrust.dao.mysql.PatientDAO;
import edu.ncsu.csc.itrust.dao.mysql.HealthRecordsDAO;
import edu.ncsu.csc.itrust.unit.datagenerators.TestDataGenerator;
import edu.ncsu.csc.itrust.unit.testutils.TestDAOFactory;
import edu.ncsu.csc.itrust.exception.FormValidationException;
import edu.ncsu.csc.itrust.exception.ITrustException;
import edu.ncsu.csc.itrust.enums.Role;


public class AddPreRegisterPatientActionTest extends TestCase {
    private DAOFactory factory = TestDAOFactory.getTestInstance();
    private PatientDAO patientDAO = TestDAOFactory.getTestInstance().getPatientDAO();
    private AuthDAO authDAO = TestDAOFactory.getTestInstance().getAuthDAO();
    private HealthRecordsDAO healthDAO = TestDAOFactory.getTestInstance().getHealthRecordsDAO();
    private TestDataGenerator gen = new TestDataGenerator();
	private AddPreRegisteredPatientAction action;
    
    private HealthRecordForm defaultHRF;

/**
 * Sets up defaults
 */
	@Override
	protected void setUp() throws Exception {
        gen.clearAllTables();
        action = new AddPreRegisteredPatientAction(factory, 0L);
        
        defaultHRF = new HealthRecordForm();
        defaultHRF.setIsSmoker("1");
        defaultHRF.setHeight("");
        defaultHRF.setWeight("");
    }
    
    /**
     * Test adding a patient with correct information.
     */
    public void testPreRegisterPatientAction() throws Exception {
        PatientBean p = new PatientBean();
        p.setFirstName("Jiminy");
		p.setLastName("Cricket");
        p.setEmail("make.awish@gmail.com");
        p.setPassword("password");
        p.setStreetAddress1("SA1");
        p.setStreetAddress2("SA2");
        p.setZip("12345");
        p.setCity("Champaign");
        p.setState("IL");
        p.setPhone("1234567890");
        p.setIcAddress1("ICA1");
        p.setIcAddress2("ICA2");
        p.setIcZip("54321");
        p.setIcCity("Urbana");
        p.setIcState("AK");
        p.setIcPhone("1122334455");

        HealthRecordForm h = new HealthRecordForm();
        h.setHeight("100");
        h.setWeight("100");
        h.setIsSmoker("1");

        long mid = action.addPatient(p, h);

        PatientBean p2 = patientDAO.getPatient(mid);
        assertEquals(p.getFirstName(), p2.getFirstName());
		assertEquals(p.getLastName(), p2.getLastName());
        assertEquals(p.getEmail(), p2.getEmail());
        assertEquals(p.getStreetAddress1(), p2.getStreetAddress1());
        assertEquals(p.getStreetAddress2(), p2.getStreetAddress2());
        assertEquals(p.getZip(), p2.getZip());
        assertEquals(p.getCity(), p2.getCity());
        assertEquals(p.getState(), p2.getState());
        assertEquals(p.getPhone(), p2.getPhone());
        assertEquals(p.getIcAddress1(), p2.getIcAddress1());
        assertEquals(p.getIcAddress2(), p2.getIcAddress2());
        assertEquals(p.getIcZip(), p2.getIcZip());
        assertEquals(p.getIcCity(), p2.getIcCity());
        assertEquals(p.getIcState(), p.getIcState());
        assertEquals(p.getIcPhone(), p.getIcPhone());

        HealthRecord h2 = healthDAO.getAllHealthRecords(mid).get(0);
        assertEquals(Double.parseDouble(h.getHeight()), h2.getHeight());
        assertEquals(Double.parseDouble(h.getWeight()), h2.getWeight());
        assertEquals(h.getIsSmoker() == "1", h2.isSmoker());
        
        assertEquals(Role.PREREGISTEREDPATIENT, authDAO.getUserRole(mid));
    }

    /**
     * Ensure that invalid emails are not allowed
     */
    public void testPreRegisterPatientInvalidEmail() throws Exception {
        PatientBean p = new PatientBean();
        p.setFirstName("Jiminy");
		p.setLastName("Cricket");
        p.setEmail("1234");
        p.setPassword("password");

        try {
            action.addPatient(p, defaultHRF);
            fail("Invalid email");
        } catch (FormValidationException e) {

        }
    }

    /**
     * Ensure that invalid heights and weights are not allowed
     */
    public void testPreRegisterPatientInvalidHeightWeight() throws Exception {
        PatientBean p = new PatientBean();
        p.setFirstName("Jiminy");
		p.setLastName("Cricket");
        p.setEmail("email@email.com");
        p.setPassword("password");

        HealthRecordForm h = new HealthRecordForm();
        h = new HealthRecordForm();
        h.setIsSmoker("0");
        h.setHeight("abc");
        h.setWeight("100");

        try {
            action.addPatient(p, h);
            fail("Expected exception");
        } catch (FormValidationException e) {

        }

        h.setHeight("100");
        h.setWeight("abc");

        try {
            action.addPatient(p, h);
            fail("Expected exception");
        } catch (FormValidationException e) {

        }
    }

    /**
     * Ensure that duplicate emails are not allowed
     */
    public void testPreRegisterPatientDuplicateEmail() throws Exception {
        PatientBean p2 = new PatientBean();
        p2.setFirstName("Jiminy");
		p2.setLastName("Cricket");
        p2.setEmail("make.awish@gmail.com");
        p2.setPassword("password");
        try {
        action.addPatient(p2, defaultHRF);
        } catch (Exception e) {e.printStackTrace();}

        PatientBean p3 = new PatientBean();
        p3.setFirstName("Make");
		p3.setLastName("AWish");
        p3.setEmail("make.awish@gmail.com");
        p3.setPassword("password");
        
        try {
            action.addPatient(p3, defaultHRF);
            fail("Duplicate email");
        } catch (ITrustException e) { }
    }
}
