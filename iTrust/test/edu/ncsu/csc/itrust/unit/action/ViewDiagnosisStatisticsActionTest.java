package edu.ncsu.csc.itrust.unit.action;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Date;
import java.text.SimpleDateFormat;

import edu.ncsu.csc.itrust.beans.DiagnosisBean;
import edu.ncsu.csc.itrust.beans.DiagnosisStatisticsBean;
import edu.ncsu.csc.itrust.exception.DBException;
import edu.ncsu.csc.itrust.exception.FormValidationException;
import edu.ncsu.csc.itrust.unit.datagenerators.TestDataGenerator;
import edu.ncsu.csc.itrust.unit.testutils.TestDAOFactory;
import edu.ncsu.csc.itrust.action.*;
import junit.framework.TestCase;

@SuppressWarnings("unused")
public class ViewDiagnosisStatisticsActionTest extends TestCase {

	private TestDataGenerator gen = new TestDataGenerator();
	private ViewDiagnosisStatisticsAction action;
	
	private int thisYear = Calendar.getInstance().get(Calendar.YEAR);

	
	protected void setUp() throws Exception {
		gen.clearAllTables();
		gen.standardData();
		gen.patient_hcp_vists();
		gen.hcp_diagnosis_data();
		
		action = new ViewDiagnosisStatisticsAction(TestDAOFactory.getTestInstance());
	}
	
	public void testGetDiagnosisCodes() throws Exception {
		List<DiagnosisBean> db = action.getDiagnosisCodes();
		assertEquals(19, db.size());
	}
	
	public void testGetDiagnosisStatisticsValid() throws Exception {
		DiagnosisStatisticsBean dsBean = action.getDiagnosisStatistics("06/28/2011", "09/28/2011", "487.00", "27606-1234");
		assertEquals(2, dsBean.getZipStats());
		assertEquals(5, dsBean.getRegionStats());
	}
	
	public void testGetDiagnosisStatisticsValidNull() throws Exception {
		DiagnosisStatisticsBean dsBean = action.getDiagnosisStatistics(null, null, "487.00", "27606");
		assertEquals(null, dsBean);
	}
	
	public void testGetDiagnosisStatisticsInvalidDate() throws Exception {
		try {
			action.getDiagnosisStatistics("06-28/2011", "09/28/2011", "487.00", "27606");
			fail("Should have failed but didn't");
		} catch (FormValidationException e) {
			assertEquals(1, e.getErrorList().size());
			assertEquals("Enter dates in MM/dd/yyyy", e.getErrorList().get(0));
		}
	}
	
	
	public void testGetDiagnosisStatisticsReversedDates() throws Exception {
		try {
			action.getDiagnosisStatistics("09/28/2011", "06/28/2011", "487.00", "27606");
			fail("Should have failed but didn't");
		} catch (FormValidationException e) {
			assertEquals(1, e.getErrorList().size());
			assertEquals("Start date must be before end date!", e.getErrorList().get(0));
		}
	}

	public void testGetDiagnosisStatisticsCorrectDerivedStartDate() throws Exception {
		DiagnosisStatisticsBean dsBean = action.getDiagnosisStatistics("11/09/2020", "487.00", "27606");
		SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		Date beginDate = dsBean.getStartDate();
		String startDate = dateFormat.format(beginDate);
		assertEquals("09/14/2020", startDate);
	}

	public void testGetDiagnosisStatisticsInvalidEndDate() throws Exception {
		try {
			action.getDiagnosisStatistics("06-28/2011","487.00", "27606");
			fail("Should have failed but didn't");
		} catch (FormValidationException e) {
			assertEquals(1, e.getErrorList().size());
			assertEquals("Enter date in MM/dd/yyyy", e.getErrorList().get(0));
		}
	}
	
	public void testGetDiagnosisStatisticsInvalidZip() throws Exception {
		try {
			action.getDiagnosisStatistics("06/28/2011", "09/28/2011", "487.00", "2766");
			fail("Should have failed but didn't");
		} catch (FormValidationException e) {
			assertEquals(1, e.getErrorList().size());
			assertEquals("Zip Code must be 5 digits!", e.getErrorList().get(0));
		}
	}
	
	public void testGetDiagnosisStatisticsInvalidICDCode() throws Exception {
		try {
			action.getDiagnosisStatistics("06/28/2011", "09/28/2011", "11114.00", "27606");
			fail("Should have failed but didn't");
		} catch (FormValidationException e) {
			assertEquals(1, e.getErrorList().size());
			assertEquals("ICDCode must be valid diagnosis!", e.getErrorList().get(0));
		}
	}
	
	public void testIsMalariaEpidemicBasic() throws Exception {
		gen.malaria_epidemic1();
		assertFalse(action.isMalariaEpidemic("11/02/" + thisYear, "27606", "110"));
		assertTrue(action.isMalariaEpidemic("11/16/" + thisYear, "27606", "110"));
	}
	
	public void testIsMalariaEpidemicRolling() throws Exception {
		int year = thisYear-20;
		gen.malaria_epidemic2();
		assertFalse(action.isMalariaEpidemic("11/01/" + year, "27606", "110"));
		assertFalse(action.isMalariaEpidemic("11/02/" + year, "27606", "110"));
		assertFalse(action.isMalariaEpidemic("11/03/" + year, "27606", "110"));
		assertFalse(action.isMalariaEpidemic("11/04/" + year, "27606", "110"));
		assertFalse(action.isMalariaEpidemic("11/05/" + year, "27606", "110"));
		assertFalse(action.isMalariaEpidemic("11/06/" + year, "27606", "110"));
		assertFalse(action.isMalariaEpidemic("11/07/" + year, "27606", "110"));
		assertFalse(action.isMalariaEpidemic("11/08/" + year, "27606", "110"));
		assertFalse(action.isMalariaEpidemic("11/09/" + year, "27606", "110"));
		assertFalse(action.isMalariaEpidemic("11/10/" + year, "27606", "110"));
		assertFalse(action.isMalariaEpidemic("11/11/" + year, "27606", "110"));
		assertFalse(action.isMalariaEpidemic("11/12/" + year, "27606", "110"));
		assertFalse(action.isMalariaEpidemic("11/13/" + year, "27606", "110"));
		assertTrue(action.isMalariaEpidemic("11/14/" + year, "27606", "110"));
		assertTrue(action.isMalariaEpidemic("11/15/" + year, "27606", "110"));
	}

	public void testIsMalariaEpidemicEdge() throws Exception {
		int year = thisYear - 20;
		gen.malaria_epidemic3();
		// Exactly 100 percentile
		assertFalse(action.isMalariaEpidemic("11/02/" + thisYear, "27606", "110"));

		// Threshold shouldn't matter if there is no history
		assertTrue(action.isMalariaEpidemic("10/10/" + year, "27606", "110"));
		assertTrue(action.isMalariaEpidemic("10/10/" + year, "27606", "100000000"));

		// Date/zip with 0 cases should be false
		assertFalse(action.isMalariaEpidemic("02/02/" + thisYear, "27606", "110"));
		assertFalse(action.isMalariaEpidemic("10/10/" + thisYear, "99999", "110"));
	}

	public void testIsMalariaEpidemicNone() throws Exception {
		gen.malaria_epidemic1();
		assertFalse(action.isMalariaEpidemic("06/02/2010", "38201", "110"));
	}
	
	public void testIsFluEpidemic() throws Exception {
		gen.influenza_epidemic();
		assertTrue(action.isFluEpidemic("11/16/" + thisYear, "27606"));
		assertFalse(action.isFluEpidemic("11/09/" + thisYear, "27606"));
	}

	public void testIsFluEpidemicEdge() throws Exception {
		gen.influenza_epidemic();
		// Test should pass since previous 14 days will have enough cases for epidemic
		assertTrue(action.isFluEpidemic("11/15/" + thisYear, "27606"));
		// Test should fail since previous 14 days will not have enough cases for epidemic
		assertFalse(action.isFluEpidemic("11/17/" + thisYear, "27606"));
	}
	
	public void testGetEpidemicStatisticsInvalidThreshold(){
		try{
			ArrayList<DiagnosisStatisticsBean> dsList = action.getEpidemicStatistics("11/02/" + thisYear, "84.50", "27606", "");
			fail("FormValidationException should have been thrown.");
		}catch(FormValidationException e){
			//This should be thrown
		} catch (DBException e) {
			fail("DB Exception thrown");
		}
	}
	
	public void testGetEpidemicStatistics() {
		try {
			ArrayList<DiagnosisStatisticsBean> dsList = 
					action.getEpidemicStatistics("11/02/2012", "487.00", 
							"00601", "5");
			assertEquals(2, dsList.size());
		} catch (FormValidationException e) {
			fail("FormValidationException");
		} catch (DBException d) {
			fail("DBException thrown");
		}
	}
	
	public void testGetEpidemicStatisticsInvalidDate() throws Exception {
        // Invalid format
		try {
			action.getEpidemicStatistics("20/20/2020", "84.50", "27607", "110");
			fail("Expected exception.");
        } catch (FormValidationException e) { }
        
        // Future date
        try {
			action.getEpidemicStatistics("12/20/2021", "84.50", "27607", "110");
			fail("Expected exception.");
        } catch (FormValidationException e) { }
	}
	
	public void testGetDiagnosisTrendsBasic() throws Exception {
		gen.hcp_additional_diagnosis_data();
		List<DiagnosisStatisticsBean> trends = action.getDiagnosisTrends("03/11/2007", "79.10", "27607");

		assertEquals(8, trends.size());
		assertEquals(2, trends.get(0).getRegionStats());
		assertEquals(2, trends.get(0).getStateStats());
		assertEquals(1, trends.get(0).getZipStats());
	}

	public void testGetDiagnosisTrendsRolling() throws Exception {
		gen.hcp_additional_diagnosis_data();
		List<DiagnosisStatisticsBean> trends = action.getDiagnosisTrends("03/16/2007", "79.10", "27607");

		assertEquals(2, trends.get(0).getRegionStats());
		assertEquals(0, trends.get(1).getRegionStats());
		assertEquals(0, trends.get(2).getRegionStats());

		trends = action.getDiagnosisTrends("03/17/2007", "79.10", "27607");

		assertEquals(1, trends.get(0).getRegionStats());
		assertEquals(1, trends.get(1).getRegionStats());
		assertEquals(0, trends.get(2).getRegionStats());

		trends = action.getDiagnosisTrends("03/18/2007", "79.10", "27607");
		assertEquals(0, trends.get(0).getRegionStats());
		assertEquals(2, trends.get(1).getRegionStats());
		assertEquals(0, trends.get(2).getRegionStats());
	}

	public void testGetDiagnosisTrendsRegionSize() throws Exception {
		gen.hcp_additional_diagnosis_data();
		List<DiagnosisStatisticsBean> trends = action.getDiagnosisTrends("11/19/2020", "34.00", "27607");

		assertEquals(1, trends.get(0).getZipStats());
		assertEquals(5, trends.get(0).getRegionStats());
		assertEquals(6, trends.get(0).getStateStats());
	}

	public void testGetDiagnosisTrendsEdge() throws Exception {
		List<DiagnosisStatisticsBean> trends = action.getDiagnosisTrends("12/18/2010", "79.10", "27607");

		for (DiagnosisStatisticsBean b : trends) {
			assertEquals(0, b.getZipStats());
			assertEquals(0, b.getRegionStats());
			assertEquals(0, b.getStateStats());
		}
	}

	public void testGetDiagnosisTrendsBadDate() throws Exception {
        // Random chars
		try {
			action.getDiagnosisTrends("asdf", "79.10", "27607");
			fail("Expected exception.");
		} catch (FormValidationException e) { }
        
        // Bad format
		try {
			action.getDiagnosisTrends("20/20/2010", "79.10", "27607");
			fail("Expected exception.");
        } catch (FormValidationException e) { }
        
        // Future date
        try {
			action.getDiagnosisTrends("12/20/2021", "79.10", "27607");
			fail("Expected exception.");
        } catch (FormValidationException e) { }
	}

	public void testGetDiagnosisTrendsBadZip() throws Exception {
		try {
			action.getDiagnosisTrends("03/20/2007", "79.10", "123456");
			fail("Expected exception.");
		} catch (FormValidationException e) { }
	}

	public void testGetDiagnosisTrendsBadIcdCode() throws Exception {
		try {
			action.getDiagnosisTrends("03/20/2007", "000.000", "12345");
			fail("Expected exception.");
		} catch (FormValidationException e) { }
	}
}
