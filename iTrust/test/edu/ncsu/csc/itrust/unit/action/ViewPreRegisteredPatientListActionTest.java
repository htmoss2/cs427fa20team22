package edu.ncsu.csc.itrust.unit.action;

import junit.framework.TestCase;
import edu.ncsu.csc.itrust.action.ViewPreRegisteredPatientListAction;
import edu.ncsu.csc.itrust.beans.PatientBean;
import edu.ncsu.csc.itrust.dao.DAOFactory;
import edu.ncsu.csc.itrust.unit.datagenerators.TestDataGenerator;
import edu.ncsu.csc.itrust.unit.testutils.TestDAOFactory;

import java.util.List;

public class ViewPreRegisteredPatientListActionTest extends TestCase {

	private DAOFactory factory = TestDAOFactory.getTestInstance();
	private ViewPreRegisteredPatientListAction action;
	private TestDataGenerator gen;

	@Override
	protected void setUp() throws Exception {
		gen = new TestDataGenerator();
		gen.clearAllTables();
	}

	public void testViewPatients() throws Exception {
		long hcpMID = 900000000;
		action = new ViewPreRegisteredPatientListAction(factory, hcpMID);
		List<PatientBean> pb = action.getPreRegisteredPatients();
		assertEquals(0, pb.size());

		pb = action.getPreRegisteredPatients();
		assertEquals(0, pb.size());
	}

	public void testNoPersonnel() throws Exception {

		long hcpMID = 900000000;

		action = new ViewPreRegisteredPatientListAction(factory, hcpMID);
		List<PatientBean> pb = action.getPreRegisteredPatients();
		for (PatientBean p : pb) {
			if (p.getMID() == 90000000) {
				fail("This id should not exist in the list");
			}
		}

		return;
	}
}
