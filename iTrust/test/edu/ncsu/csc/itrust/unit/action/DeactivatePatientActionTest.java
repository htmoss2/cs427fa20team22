package edu.ncsu.csc.itrust.unit.action;

import edu.ncsu.csc.itrust.action.EditPatientAction;
import edu.ncsu.csc.itrust.dao.mysql.PatientDAO;
import edu.ncsu.csc.itrust.unit.datagenerators.TestDataGenerator;
import edu.ncsu.csc.itrust.unit.testutils.TestDAOFactory;
import junit.framework.TestCase;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class DeactivatePatientActionTest extends TestCase {

	private TestDataGenerator gen = new TestDataGenerator();
	private PatientDAO patientDAO = TestDAOFactory.getTestInstance().getPatientDAO();

	@Override
	protected void setUp() throws Exception {
		gen.clearAllTables();
		gen.patient92();
		gen.patient4();
	}

	public void testDeactivateActivatedPatient() throws Exception {
		long pid = 4;
		assertEquals("patient", patientDAO.getRole(pid));
		EditPatientAction action = new EditPatientAction(TestDAOFactory.getTestInstance(), pid, Long.toString(pid));
		action.deactivate();
		assertEquals("deactivatedPatient", patientDAO.getRole(pid));
		assertEquals(new SimpleDateFormat("MM/dd/yyyy").format(Calendar.getInstance().getTime()), patientDAO.getPatient(pid).getDateOfDeactivationStr());
	}

	public void testDeactivatePreregisteredPatient() throws Exception {
		long pid = 92;
		assertEquals("preRegisteredPatient", patientDAO.getRole(pid));
		EditPatientAction action = new EditPatientAction(TestDAOFactory.getTestInstance(), pid, Long.toString(pid));
		action.deactivate();
		assertEquals("deactivatedPatient", patientDAO.getRole(pid));
		assertEquals(new SimpleDateFormat("MM/dd/yyyy").format(Calendar.getInstance().getTime()), patientDAO.getPatient(pid).getDateOfDeactivationStr());
	}
}
