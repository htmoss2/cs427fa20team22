package edu.ncsu.csc.itrust.unit.bean;

import junit.framework.TestCase;
import java.sql.Timestamp;
import java.util.Date;
import edu.ncsu.csc.itrust.enums.TransactionType;
import edu.ncsu.csc.itrust.beans.TransactionWithRoleBean;

public class TransactionWithRoleBeanTest extends TestCase {
	
	public void testTransactionWithRoleBeanRoles() throws Exception {
		TransactionWithRoleBean b = new TransactionWithRoleBean();
		
		// test setters
		long MID1 = 23;
		long MID2 = 7000;
		long TID = 5;
		b.setMainRole("hcp");
		b.setSecondaryRole("patient");
		b.setLoggedInMID(MID1);
		b.setSecondaryMID(MID2);
		b.setTransactionID(TID);
		
		// confirm with getters
		assertEquals("hcp", b.getMainRole());
		assertEquals("patient", b.getSecondaryRole());
		assertEquals(MID1, b.getLoggedInMID());
		assertEquals(MID2, b.getSecondaryMID());
		assertEquals(TID, b.getTransactionID());
	}

	public void testTransactionWithRoleBeanTimes() throws Exception {
		TransactionWithRoleBean b = new TransactionWithRoleBean();
		
		// test setters
        Date date= new Date();
        long time = date.getTime();
        Timestamp ts = new Timestamp(time);
		b.setTimeLogged(ts);
		b.setTransactionType(TransactionType.LOGIN_FAILURE);
		b.setConvertedDate("01/01/2000");
		
		// confirm with getters
		assertEquals(ts, b.getTimeLogged());
		assertEquals(TransactionType.LOGIN_FAILURE, b.getTransactionType());
		assertEquals("01/01/2000", b.getConvertedDate());
	}

	public void testTransactionWithRoleBeanInfo() throws Exception {
		TransactionWithRoleBean b = new TransactionWithRoleBean();
		
		// test setters
        b.setAddedInfo("I'm dying");

		// confirm with getters
		assertEquals("I'm dying", b.getAddedInfo());
	}

}
