package edu.ncsu.csc.itrust.unit.dao.patient;

import edu.ncsu.csc.itrust.action.EditPatientAction;
import edu.ncsu.csc.itrust.dao.mysql.PatientDAO;
import edu.ncsu.csc.itrust.unit.datagenerators.TestDataGenerator;
import edu.ncsu.csc.itrust.unit.testutils.TestDAOFactory;
import junit.framework.TestCase;

public class ActivatePatientTest extends TestCase {
	private TestDataGenerator gen = new TestDataGenerator();
	private PatientDAO patientDAO = TestDAOFactory.getTestInstance().getPatientDAO();

	@Override
	protected void setUp() throws Exception {
		gen.clearAllTables();
		gen.patient92();
		gen.patient4();
	}

	public void testActivateNewPatient() throws Exception {
		long pid = 92;
		assertEquals("preRegisteredPatient", patientDAO.getRole(pid));
		patientDAO.ConvertPreRegisteredPatientsToPatient(pid);
		assertEquals("patient", patientDAO.getRole(pid));
	}

	public void testActivateActionPatient() throws Exception {
		long pid = 92;
		assertEquals("preRegisteredPatient", patientDAO.getRole(pid));
		EditPatientAction action = new EditPatientAction(TestDAOFactory.getTestInstance(), pid, Long.toString(pid));
		action.activate();
		assertEquals("patient", patientDAO.getRole(pid));
	}
}
