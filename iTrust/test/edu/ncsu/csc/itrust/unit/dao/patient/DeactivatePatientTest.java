package edu.ncsu.csc.itrust.unit.dao.patient;

import edu.ncsu.csc.itrust.dao.mysql.PatientDAO;
import edu.ncsu.csc.itrust.unit.datagenerators.TestDataGenerator;
import edu.ncsu.csc.itrust.unit.testutils.TestDAOFactory;
import junit.framework.TestCase;

public class DeactivatePatientTest extends TestCase {

	private TestDataGenerator gen = new TestDataGenerator();
	private PatientDAO patientDAO = TestDAOFactory.getTestInstance().getPatientDAO();

	@Override
	protected void setUp() throws Exception {
		gen.clearAllTables();
		gen.patient92();
		gen.patient4();
	}

	public void testDeactivateActivatedPatient() throws Exception {
		long pid = 4;
		assertEquals("patient", patientDAO.getRole(pid));
		patientDAO.deactivatePatient(pid);
		assertEquals("deactivatedPatient", patientDAO.getRole(pid));
	}

	public void testDeactivatePreregisteredPatient() throws Exception {
		long pid = 92;
		assertEquals("preRegisteredPatient", patientDAO.getRole(pid));
		patientDAO.deactivatePatient(pid);
		assertEquals("deactivatedPatient", patientDAO.getRole(pid));
	}

	public void testDeactivateThenActivatePatient() throws Exception {
		long pid = 92;
		assertEquals("preRegisteredPatient", patientDAO.getRole(pid));
		patientDAO.ConvertPreRegisteredPatientsToPatient(pid);
		assertEquals("patient", patientDAO.getRole(pid));
		patientDAO.deactivatePatient(pid);
		assertEquals("deactivatedPatient", patientDAO.getRole(pid));
	}
}
