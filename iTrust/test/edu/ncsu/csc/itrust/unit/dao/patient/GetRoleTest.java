package edu.ncsu.csc.itrust.unit.dao.patient;

import edu.ncsu.csc.itrust.dao.mysql.PatientDAO;
import edu.ncsu.csc.itrust.unit.datagenerators.TestDataGenerator;
import edu.ncsu.csc.itrust.unit.testutils.TestDAOFactory;
import junit.framework.TestCase;

public class GetRoleTest extends TestCase {
	private PatientDAO patientDAO = TestDAOFactory.getTestInstance().getPatientDAO();
	private TestDataGenerator gen;

	@Override
	protected void setUp() throws Exception {
		gen = new TestDataGenerator();
		gen.clearAllTables();
		gen.patient92();
		gen.patient4();
		gen.patient21();
	}

	public void testGetRole() throws Exception {
		String role = patientDAO.getRole(92);
		assertEquals("preRegisteredPatient", role);
	}

	public void testAnotherRole() throws Exception {
		String role = patientDAO.getRole(4);
		assertEquals("patient", role);
	}

	public void testNotRole() throws Exception {
		String role = patientDAO.getRole(21);
		assertEquals("patient", role);
	}

	public void testNonExistent() throws Exception {
		try {
			String role = patientDAO.getRole(9999999);
			fail("This shouldn't ever be reached");
		} catch (Exception e) {
			assertEquals("User does not exist with the designated role", e.getMessage());
		}
	}
	
}
