package edu.ncsu.csc.itrust.unit.dao.deaths;

import edu.ncsu.csc.itrust.beans.CauseOfDeathTrendsReportBean;
import edu.ncsu.csc.itrust.dao.mysql.CauseOfDeathTrendsReportDAO;
import edu.ncsu.csc.itrust.exception.DBException;
import edu.ncsu.csc.itrust.unit.datagenerators.TestDataGenerator;
import edu.ncsu.csc.itrust.unit.testutils.EvilDAOFactory;
import edu.ncsu.csc.itrust.unit.testutils.TestDAOFactory;
import junit.framework.TestCase;

import java.util.List;


public class CauseOfDeathTrendsReportTest extends TestCase {
    
    private CauseOfDeathTrendsReportDAO cod;

    private CauseOfDeathTrendsReportDAO evilDAO = new CauseOfDeathTrendsReportDAO(EvilDAOFactory.getEvilInstance());
    
    @Override
	protected void setUp() throws Exception {
		TestDataGenerator gen = new TestDataGenerator();
		gen.clearAllTables();
        gen.deadPatient1();
        gen.deadPatient2();
        gen.deadPatient3();
        gen.deadPatient4();
        gen.deadPatient5();
        gen.deadPatient6();
        gen.deadPatient7();
        gen.deadPatient8();
        gen.deadPatient9();
        gen.deadPatient10();
        gen.icd9cmCodes();

        cod = new CauseOfDeathTrendsReportDAO(TestDAOFactory.getTestInstance());
    }
    
    public void testPatientsForHCPValid() throws Exception {
        List<Long> deaths = cod.getPatientsForHCP(Long.parseLong("9000000000"));
        assertEquals(6, deaths.size());
        assertEquals(Long.valueOf(81), deaths.get(0));
        assertEquals(Long.valueOf(82), deaths.get(1));
        assertEquals(Long.valueOf(83), deaths.get(2));
        assertEquals(Long.valueOf(84), deaths.get(3));
        assertEquals(Long.valueOf(85), deaths.get(4));
        assertEquals(Long.valueOf(86), deaths.get(5));
    }

    public void testPatientsForHCPException() throws Exception {
		try {
			evilDAO.getPatientsForHCP(Long.parseLong("0000000000"));
			fail("DBException should have been thrown");
		} catch (DBException e) {
			assertEquals(EvilDAOFactory.MESSAGE, e.getSQLException().getMessage());
		}
	}

    public void testCodeNameExists() throws Exception {
        String deaths = cod.getCodeName("84.50");
        assertEquals("Malaria", deaths);
    }

    public void testCodeNameDoesNotExist() throws Exception {
        String deaths = cod.getCodeName("8450");
        assertEquals(null, deaths);
    }
    
    public void testTopTwoDeathsForHCPValid() throws Exception {
        List<CauseOfDeathTrendsReportBean> deaths = cod.getTopTwoDeathsForHCP(Long.parseLong("9000000000"), "All", "01/01/2018", "12/31/2020");
        assertEquals(2, deaths.size());
        assertEquals("11.40", deaths.get(0).getCode());
        assertEquals("Tuberculosis of the lung", deaths.get(0).getCause());
        assertEquals("3", deaths.get(0).getDeaths());
        assertEquals("84.50", deaths.get(1).getCode());
        assertEquals("Malaria", deaths.get(1).getCause());
        assertEquals("2", deaths.get(1).getDeaths());
    }

    public void testTopTwoDeathsForHCPMaleValid() throws Exception {
        List<CauseOfDeathTrendsReportBean> deaths = cod.getTopTwoDeathsForHCP(Long.parseLong("9000000000"), "Male", "01/01/2018", "12/31/2020");
        assertEquals(2, deaths.size());
        assertEquals("84.50", deaths.get(0).getCode());
        assertEquals("Malaria", deaths.get(0).getCause());
        assertEquals("2", deaths.get(0).getDeaths());
        assertEquals("11.40", deaths.get(1).getCode());
        assertEquals("Tuberculosis of the lung", deaths.get(1).getCause());
        assertEquals("2", deaths.get(1).getDeaths());
    }

    public void testTopTwoDeathsForHCPException() throws Exception {
		try {
			evilDAO.getTopTwoDeathsForHCP(Long.parseLong("0000000000"), "All", "01/01/1990", "12/31/2020");
			fail("DBException should have been thrown");
		} catch (DBException e) {
			assertEquals(EvilDAOFactory.MESSAGE, e.getSQLException().getMessage());
		}
    }
    
    public void testTopTwoDeathsForHCPNullDate() throws Exception {
        List<CauseOfDeathTrendsReportBean> deaths = cod.getTopTwoDeathsForHCP(Long.parseLong("9000000000"), "Male", null, "12/31/2020");
        assertEquals(null, deaths);
    }

    public void testTopTwoDeathsForAllValid() throws Exception {
        List<CauseOfDeathTrendsReportBean> deaths = cod.getTopTwoDeathsForAll("All", "01/01/2018", "12/31/2020");
        assertEquals(2, deaths.size());
        assertEquals("487.00", deaths.get(0).getCode());
        assertEquals("Influenza", deaths.get(0).getCause());
        assertEquals("5", deaths.get(0).getDeaths());
        assertEquals("11.40", deaths.get(1).getCode());
        assertEquals("Tuberculosis of the lung", deaths.get(1).getCause());
        assertEquals("3", deaths.get(1).getDeaths());
    }

    public void testTopTwoDeathsForAllFemaleValid() throws Exception {
        List<CauseOfDeathTrendsReportBean> deaths = cod.getTopTwoDeathsForAll("Female", "01/01/2018", "12/31/2020");
        assertEquals(2, deaths.size());
        assertEquals("487.00", deaths.get(0).getCode());
        assertEquals("Influenza", deaths.get(0).getCause());
        assertEquals("2", deaths.get(0).getDeaths());
        assertEquals("11.40", deaths.get(1).getCode());
        assertEquals("Tuberculosis of the lung", deaths.get(1).getCause());
        assertEquals("1", deaths.get(1).getDeaths());
    }

    public void testTopTwoDeathsForAllNullDate() throws Exception {
        List<CauseOfDeathTrendsReportBean> deaths = cod.getTopTwoDeathsForAll("Male", null, "12/31/2020");
        assertEquals(null, deaths);
    }
}