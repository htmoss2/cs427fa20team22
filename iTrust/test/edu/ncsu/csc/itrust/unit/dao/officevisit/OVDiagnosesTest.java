package edu.ncsu.csc.itrust.unit.dao.officevisit;

import edu.ncsu.csc.itrust.action.ViewDiagnosisStatisticsAction;
import edu.ncsu.csc.itrust.beans.DiagnosisBean;
import edu.ncsu.csc.itrust.beans.DiagnosisStatisticsBean;
import edu.ncsu.csc.itrust.dao.DAOFactory;
import edu.ncsu.csc.itrust.dao.mysql.DiagnosesDAO;
import edu.ncsu.csc.itrust.exception.DBException;
import edu.ncsu.csc.itrust.unit.datagenerators.TestDataGenerator;
import edu.ncsu.csc.itrust.unit.testutils.TestDAOFactory;
import junit.framework.TestCase;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Test client diagnoses
 */
public class OVDiagnosesTest extends TestCase {
	private DiagnosesDAO diagDAO = TestDAOFactory.getTestInstance().getDiagnosesDAO();
	private int thisYear = Calendar.getInstance().get(Calendar.YEAR);
	
	@Override
	protected void setUp() throws Exception {
		TestDataGenerator gen = new TestDataGenerator();
		gen.clearAllTables();
		
		gen.standardData();
		gen.patient_hcp_vists();
		gen.hcp_diagnosis_data();
	}

	/**
	 * testAddRemoveOneOVDiagnosis
	 * @throws Exception
	 */
	public void testAddRemoveOneOVDiagnosis() throws Exception {
		assertEquals("no current diagnoses on office vist 1", 0, diagDAO.getList(1).size());
		DiagnosisBean bean = new DiagnosisBean();
		bean.setICDCode("250.1");
		bean.setVisitID(1);
		long ovDID = diagDAO.add(bean);
		List<DiagnosisBean> diagnoses = diagDAO.getList(1);
		assertEquals("now there's 1", 1, diagnoses.size());
		assertEquals("test the description", "Diabetes with ketoacidosis", diagnoses.get(0).getDescription());
		diagDAO.remove(ovDID);
		assertEquals("now there's none", 0, diagDAO.getList(1).size());
	}

	/**
	 * testAddBadDiagnosis
	 * @throws Exception
	 */
	public void testAddBadDiagnosis() throws Exception {
		DiagnosisBean bean = new DiagnosisBean();
		bean.setVisitID(-1);
		bean.setICDCode(null);
		try {
			diagDAO.add(bean);
			fail("expected an exception");
		} catch (DBException e) {
			//TODO
		}
	}
	
	/**
	 * testEditBadDiagnosis
	 * @throws Exception
	 */
	public void testEditBadDiagnosis() throws Exception {
		DiagnosisBean bean = new DiagnosisBean();
		bean.setVisitID(-1);
		bean.setICDCode(null);
		try {
			diagDAO.edit(bean);
		} catch (DBException e) {
			//TODO
		}
	}

	public void testGetStatisticsValidBasic() throws Exception {
		Date lower = new SimpleDateFormat("MM/dd/yyyy").parse("06/28/2011");
		Date upper = new SimpleDateFormat("MM/dd/yyyy").parse("09/28/2011");
		DiagnosisStatisticsBean dsBean = diagDAO.getDiagnosisCounts("487.00", "27607", lower, upper);
		assertEquals(3, dsBean.getZipStats());
		assertEquals(5, dsBean.getRegionStats());
	}

	public void testGetStatisticsValidAll() throws Exception {
		TestDataGenerator gen = new TestDataGenerator();
		gen.hcp_additional_diagnosis_data();
		
		Date lower = new SimpleDateFormat("MM/dd/yyyy").parse("11/01/2020");
		Date upper = new SimpleDateFormat("MM/dd/yyyy").parse("11/30/2020");
		DiagnosisStatisticsBean dsBean = diagDAO.getDiagnosisCounts("34.00", "27607", lower, upper);
		assertEquals(1, dsBean.getZipStats());
		assertEquals(5, dsBean.getRegionStats());
		assertEquals(6, dsBean.getStateStats());
		assertEquals(11, dsBean.getAllStats());
	}
	
	public void testZipSFs() throws Exception {
		TestDataGenerator gen = new TestDataGenerator();
		gen.hcp_additional_diagnosis_data();
		Date lower = new SimpleDateFormat("MM/dd/yyyy").parse("11/18/2020");
		Date upper = new SimpleDateFormat("MM/dd/yyyy").parse("11/18/2020");
		DiagnosisStatisticsBean bean = diagDAO.getDiagnosisCounts("34.00", "27606-1234", lower, upper, 5);
		assertEquals(bean.getZipStats(), bean.getRegionStats());
		assertEquals(3, bean.getZipStats());
		
		lower = new SimpleDateFormat("MM/dd/yyyy").parse("11/18/2020");
		upper = new SimpleDateFormat("MM/dd/yyyy").parse("11/18/2020");
		bean = diagDAO.getDiagnosisCounts("34.00", "27607", lower, upper, 4);
		assertEquals(1, bean.getZipStats());
		assertEquals(5, bean.getRegionStats());

		lower = new SimpleDateFormat("MM/dd/yyyy").parse("11/18/2020");
		upper = new SimpleDateFormat("MM/dd/yyyy").parse("11/18/2020");
		bean = diagDAO.getDiagnosisCounts("34.00", "27603", lower, upper, 3);
		assertEquals(1, bean.getZipStats());
		assertEquals(5, bean.getRegionStats());

		lower = new SimpleDateFormat("MM/dd/yyyy").parse("11/18/2020");
		upper = new SimpleDateFormat("MM/dd/yyyy").parse("11/18/2020");
		bean = diagDAO.getDiagnosisCounts("34.00", "27606", lower, upper, 2);
		assertEquals(0, bean.getZipStats());
		assertEquals(6, bean.getRegionStats());

		lower = new SimpleDateFormat("MM/dd/yyyy").parse("11/18/2020");
		upper = new SimpleDateFormat("MM/dd/yyyy").parse("11/18/2020");
		bean = diagDAO.getDiagnosisCounts("34.00", "20000", lower, upper, 1);
		assertEquals(0, bean.getZipStats());
		assertEquals(7, bean.getRegionStats());

		lower = new SimpleDateFormat("MM/dd/yyyy").parse("11/18/2020");
		upper = new SimpleDateFormat("MM/dd/yyyy").parse("11/18/2020");
		bean = diagDAO.getDiagnosisCounts("34.00", "10001", lower, upper, 0);
		assertEquals(3, bean.getZipStats());
		assertEquals(11, bean.getRegionStats());
	}

	public void testInvalidZipSigFigs() throws Exception {
		try {
			Date lower = new SimpleDateFormat("MM/dd/yyyy").parse("11/18/2020");
			Date upper = new SimpleDateFormat("MM/dd/yyyy").parse("11/18/2020");
			DiagnosisStatisticsBean bean = diagDAO.getDiagnosisCounts("26.8", "27606-1234", lower, upper, -1);
			fail("Expected exception.");
		} catch (IllegalArgumentException e) { }

		try {
			Date lower = new SimpleDateFormat("MM/dd/yyyy").parse("11/18/2020");
			Date upper = new SimpleDateFormat("MM/dd/yyyy").parse("11/18/2020");
			DiagnosisStatisticsBean bean = diagDAO.getDiagnosisCounts("26.8", "27606-1234", lower, upper, 6);
			fail("Expected exception.");
		} catch (IllegalArgumentException e) { }
	}

	public void testGetCountForWeekBeforeRolling() throws Exception {
		TestDataGenerator gen = new TestDataGenerator();
		gen.malaria_epidemic2();

		Calendar cal = Calendar.getInstance();
		cal.setTime(new SimpleDateFormat("MM/dd/yyyy").parse("11/01/2000"));

		gen.malaria_epidemic2();
		assertEquals(0, diagDAO.getCountForWeekBefore("84.5", "27606", cal.getTime()).getRegionStats());
		cal.add(Calendar.DATE, 1);
		assertEquals(1, diagDAO.getCountForWeekBefore("84.5", "27606", cal.getTime()).getRegionStats());
		cal.add(Calendar.HOUR, 24);
		assertEquals(2, diagDAO.getCountForWeekBefore("84.5", "27606", cal.getTime()).getRegionStats());
		cal.add(Calendar.HOUR, 24);
		assertEquals(3, diagDAO.getCountForWeekBefore("84.5", "27606", cal.getTime()).getRegionStats());
		cal.add(Calendar.HOUR, 24);
		assertEquals(4, diagDAO.getCountForWeekBefore("84.5", "27606", cal.getTime()).getRegionStats());
		cal.add(Calendar.HOUR, 24);
		assertEquals(5, diagDAO.getCountForWeekBefore("84.5", "27606", cal.getTime()).getRegionStats());
		cal.add(Calendar.HOUR, 24);
		assertEquals(6, diagDAO.getCountForWeekBefore("84.5", "27606", cal.getTime()).getRegionStats());
		cal.add(Calendar.HOUR, 24);
		assertEquals(7, diagDAO.getCountForWeekBefore("84.5", "27606", cal.getTime()).getRegionStats());
		cal.add(Calendar.HOUR, 24);
		assertEquals(7, diagDAO.getCountForWeekBefore("84.5", "27606", cal.getTime()).getRegionStats());
		cal.add(Calendar.HOUR, 24);
		assertEquals(7, diagDAO.getCountForWeekBefore("84.5", "27606", cal.getTime()).getRegionStats());
		cal.add(Calendar.HOUR, 24);
		assertEquals(7, diagDAO.getCountForWeekBefore("84.5", "27606", cal.getTime()).getRegionStats());
		cal.add(Calendar.HOUR, 24);
		assertEquals(7, diagDAO.getCountForWeekBefore("84.5", "27606", cal.getTime()).getRegionStats());
		cal.add(Calendar.HOUR, 24);
		assertEquals(7, diagDAO.getCountForWeekBefore("84.5", "27606", cal.getTime()).getRegionStats());
		cal.add(Calendar.HOUR, 24);
		assertEquals(7, diagDAO.getCountForWeekBefore("84.5", "27606", cal.getTime()).getRegionStats());
		cal.add(Calendar.HOUR, 24);
		assertEquals(7, diagDAO.getCountForWeekBefore("84.5", "27606", cal.getTime()).getRegionStats());
		cal.add(Calendar.HOUR, 24);
		assertEquals(6, diagDAO.getCountForWeekBefore("84.5", "27606", cal.getTime()).getRegionStats());
		cal.add(Calendar.HOUR, 24);
	}
	
	/**
	 * testMalaria
	 * @throws Exception
	 */
	public void testMalaria() throws Exception {
		DAOFactory factory = TestDAOFactory.getTestInstance();
		TestDataGenerator gen = new TestDataGenerator();
		gen.malaria_epidemic1();
		
		ViewDiagnosisStatisticsAction a = new ViewDiagnosisStatisticsAction(factory);
		
		assertTrue(a.isMalariaEpidemic("11/16/" + thisYear, "27607", "110"));
		
	}
}
