package edu.ncsu.csc.itrust.selenium;

import java.util.concurrent.TimeUnit;

import org.junit.*;
import org.openqa.selenium.*;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;

import edu.ncsu.csc.itrust.enums.TransactionType;

public class EditApptPatientTest extends iTrustSeleniumTest {
  private HtmlUnitDriver driver;
  private StringBuffer verificationErrors = new StringBuffer();

  @Before
  public void setUp() throws Exception {
    super.setUp();
    gen.clearAllTables();
    gen.standardData();
    driver = new HtmlUnitDriver();
    driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
  }

  @Test
  public void testSetPassedDate() throws Exception {
    gen.uc22();
    driver = (HtmlUnitDriver) login("1", "pw");
    driver.setJavascriptEnabled(true);
    assertEquals("iTrust - Patient Home", driver.getTitle());
    driver.findElement(By.linkText("View/Edit My Appointments")).click();
    assertLogged(TransactionType.APPOINTMENT_ALL_VIEW, 1L, 0L, "");
    driver.findElements(By.tagName("td")).get(5).findElement(By.tagName("a")).click();
    driver.findElement(By.name("startDate")).clear();
    driver.findElement(By.name("startDate")).sendKeys("10/10/2009");
    driver.findElement(By.name("request")).click();
    assertTrue(driver.getPageSource().contains("The scheduled date of this appointment"));
    assertTrue(driver.getPageSource().contains("has already passed"));
    assertNotLogged(TransactionType.APPOINTMENT_EDIT, 1L, 100L, "");
  }

  @Test
  public void testRemoveAppt() throws Exception {
    gen.uc22();
    driver = (HtmlUnitDriver) login("1", "pw");
    driver.setJavascriptEnabled(true);
    assertEquals("iTrust - Patient Home", driver.getTitle());
    driver.findElement(By.linkText("View/Edit My Appointments")).click();
    assertLogged(TransactionType.APPOINTMENT_ALL_VIEW, 1L, 0L, "");
    driver.findElements(By.tagName("td")).get(7).findElement(By.tagName("a")).click();
    driver.findElement(By.id("removeButton")).click();
    assertTrue(driver.getPageSource().contains("Success: Appointment removed"));
    assertLoggedNoSecondary(TransactionType.APPOINTMENT_REMOVE, 1L, 0L, "");
  }

  @Test
  public void testConflictingDate() throws Exception {
    driver = (HtmlUnitDriver) login("1", "pw");
    driver.setJavascriptEnabled(true);
    assertEquals("iTrust - Patient Home", driver.getTitle());
    driver.findElement(By.linkText("View/Edit My Appointments")).click();
    assertLogged(TransactionType.APPOINTMENT_ALL_VIEW, 1L, 0L, "");
    driver.findElements(By.tagName("td")).get(5).findElement(By.tagName("a")).click();
    assertEquals("iTrust - Appointment Requests", driver.getTitle());
    assertTrue(driver.getPageSource().contains("Kelly Doctor"));
    assertTrue(driver.getPageSource().contains("Consultation"));
    assertTrue(driver.getPageSource().contains("2020"));
    assertTrue(driver.getPageSource().contains("09"));
    assertTrue(driver.getPageSource().contains("10"));
    assertTrue(driver.getPageSource().contains("AM"));
    driver.findElement(By.name("comment")).clear();
    driver.findElement(By.name("comment")).sendKeys("New comment!");
    driver.findElement(By.name("request")).click();
    assertTrue(driver.getPageSource()
        .contains("ERROR: The appointment you requested conflicts with other existing appointments"));
    driver.findElements(By.tagName("input")).get(7).click();
    assertTrue(driver.getPageSource().contains("Your appointment request has been saved and is pending"));
    assertLogged(TransactionType.APPOINTMENT_REQUEST_SUBMITTED, 1L, 9000000000L, "");
  }

  @Test
  public void testEditAppt() throws Exception {
    driver = (HtmlUnitDriver) login("1", "pw");
    driver.setJavascriptEnabled(true);
    assertEquals("iTrust - Patient Home", driver.getTitle());
    driver.findElement(By.linkText("View/Edit My Appointments")).click();
    assertLogged(TransactionType.APPOINTMENT_ALL_VIEW, 1L, 0L, "");
    driver.findElements(By.tagName("td")).get(5).findElement(By.tagName("a")).click();
    assertEquals("iTrust - Appointment Requests", driver.getTitle());
    assertTrue(driver.getPageSource().contains("Kelly Doctor"));
    assertTrue(driver.getPageSource().contains("Consultation"));
    assertTrue(driver.getPageSource().contains("2020"));
    assertTrue(driver.getPageSource().contains("09"));
    assertTrue(driver.getPageSource().contains("10"));
    assertTrue(driver.getPageSource().contains("AM"));
    driver.findElement(By.name("comment")).clear();
    driver.findElement(By.name("comment")).sendKeys("New comment!");
    driver.findElement(By.name("startDate")).clear();
    driver.findElement(By.name("startDate")).sendKeys("12/20/2021");
    driver.findElement(By.name("request")).click();
    assertTrue(driver.getPageSource().contains("Your appointment request has been saved and is pending"));
    assertLogged(TransactionType.APPOINTMENT_REQUEST_SUBMITTED, 1L, 9000000000L, "");
  }

  @After
  public void tearDown() throws Exception {
    driver.quit();
    String verificationErrorString = verificationErrors.toString();
    if (!"".equals(verificationErrorString)) {
      fail(verificationErrorString);
    }
  }
}