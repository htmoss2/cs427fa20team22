package edu.ncsu.csc.itrust.selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import java.text.SimpleDateFormat;  
import java.util.Date; 
import java.util.List;

public class SendRemindersTest extends iTrustSeleniumTest{

    protected WebDriver driver;

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        gen.clearAllTables();
        gen.standardData();
    }

    // Test sending reminders
	public void testSendReminder() throws Exception {
		// Login as admin
        driver = login("9000000001", "pw");
        assertEquals("iTrust - Admin Home", driver.getTitle());
        driver.findElement(By.linkText("Send Appointment Reminders")).click();

        // Send reminders
        driver.findElement(By.name("withinDays")).sendKeys("10");
        driver.findElement(By.name("withinDays")).submit();
        assertEquals("Reminders were successfully sent",
        driver.findElement(By.className("iTrustMessage")).getText());

        // Create timestamp
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		Date date = new Date();
		String stamp = dateFormat.format(date);

        // Check admin reminders outbox
        driver.findElement(By.linkText("Reminder Message Outbox")).click();
        assertNotNull(driver.findElement(By.className("fancyTable")));
        assertTrue(driver.getPageSource().contains(stamp));

        // Check a reminder message
        driver.findElement(By.linkText("Read")).click();
        assertTrue(driver.getPageSource().contains(stamp));
    }

    // Test invalid number of days input for sending reminders
	public void testInvalidSendReminder() throws Exception {

        // Login as admin
        driver = login("9000000001", "pw");
        assertEquals("iTrust - Admin Home", driver.getTitle());
        driver.findElement(By.linkText("Send Appointment Reminders")).click();

        // Send reminder with negative days
        driver.findElement(By.name("withinDays")).sendKeys("-4");
        driver.findElement(By.name("withinDays")).submit();
        assertEquals("Provide a positive number",
        driver.findElement(By.className("iTrustError")).getText());
    
        // Send reminder with non-numberic days
        driver.findElement(By.name("withinDays")).sendKeys("Hello");
        driver.findElement(By.name("withinDays")).submit();
        assertEquals("Reminders failed to send. Please provide a positive number",
        driver.findElement(By.className("iTrustError")).getText());
    }



    
}