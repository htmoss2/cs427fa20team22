package edu.ncsu.csc.itrust.selenium;

import java.util.concurrent.TimeUnit;

import edu.ncsu.csc.itrust.enums.TransactionType;
import org.junit.*;
import org.openqa.selenium.*;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import com.gargoylesoftware.htmlunit.WebClient;

public class DeactivatePatientTest extends iTrustSeleniumTest {
	private WebDriver driver;
	private StringBuffer verificationErrors = new StringBuffer();

	@Before
	public void setUp() throws Exception {
		super.setUp();
		gen.standardData();
		gen.patient92();
		driver = new HtmlUnitDriver();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	}

	@Test
	public void testDeactivatePatients() throws Exception {
		HtmlUnitDriver driver = (HtmlUnitDriver)login("9000000000", "pw");

		// Make sure we were able to log in
		assertEquals("iTrust - HCP Home", driver.getTitle());
		driver.findElement(By.cssSelector("h2.panel-title")).click();
		driver.findElement(By.linkText("View Pre-Registered Patients")).click();
		driver.findElement(By.cssSelector("input.deactivate")).click();

		// Deactivate the specified patient
		driver.findElement(By.name("understand")).sendKeys("I UNDERSTAND");
		driver.findElement(By.name("action")).click();
		assertLogged(TransactionType.PREREGISTERED_PATIENT_DEACTIVATE, 9000000000L, 92L, "");

		driver.findElement(By.cssSelector("h2.panel-title")).click();
		driver.findElement(By.linkText("View Pre-Registered Patients")).click();
		String text = driver.findElement(By.cssSelector("table.fTable")).getText();

		// Ensure the deactivated patient is no longer in the preregistered patients list
		assertFalse("Text found, but shouldn't be!", text.contains("Brittany Franco"));
		assertFalse("Text found, but shouldn't be!", text.contains("1333 Who Cares Road Suite 102"));
		assertFalse("Text found, but shouldn't be!", text.contains("Raleigh NC"));
		assertFalse("Text found, but shouldn't be!", text.contains("brfranco@gmail.com"));
	}

	@Test
	public void testLoginAfterPatientDeactivated() throws Exception {
		HtmlUnitDriver driver = (HtmlUnitDriver)login("9000000000", "pw");

		// Make sure we were able to log in
		assertEquals("iTrust - HCP Home", driver.getTitle());
		driver.findElement(By.cssSelector("h2.panel-title")).click();
		driver.findElement(By.linkText("View Pre-Registered Patients")).click();
		driver.findElement(By.cssSelector("input.deactivate")).click();

		// Deactivate the specified patient
		driver.findElement(By.name("understand")).sendKeys("I UNDERSTAND");
		driver.findElement(By.name("action")).click();
		assertLogged(TransactionType.PREREGISTERED_PATIENT_DEACTIVATE, 9000000000L, 92L, "");

		driver.findElement(By.cssSelector("h2.panel-title")).click();
		driver.findElement(By.linkText("View Pre-Registered Patients")).click();
		String text = driver.findElement(By.cssSelector("table.fTable")).getText();

		// Make sure deactivated patient doesn't show up in preregistered list
		assertFalse("Text found, but shouldn't be!", text.contains("Brittany Franco"));
		assertFalse("Text found, but shouldn't be!", text.contains("1333 Who Cares Road Suite 102"));
		assertFalse("Text found, but shouldn't be!", text.contains("Raleigh NC"));
		assertFalse("Text found, but shouldn't be!", text.contains("brfranco@gmail.com"));

		// Test if the deactivated patient can log in
		HtmlUnitDriver patientDriver = (HtmlUnitDriver)login("92", "pw");
		assertEquals("Server Rebooted", patientDriver.getTitle());
		text = patientDriver.findElement(By.xpath("//body")).getText();
		assertTrue("Text not found!", text.contains("Authorization Error!"));
		assertTrue("Text not found!", text.contains("You are not allowed to access this page"));
		assertTrue("Text not found!", text.contains("Log back in."));


	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
		String verificationErrorString = verificationErrors.toString();
		if (!"".equals(verificationErrorString)) {
			fail(verificationErrorString);
		}
	}

}
