package edu.ncsu.csc.itrust.selenium;

import java.util.concurrent.TimeUnit;
import org.junit.*;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;

public class ViewTransactionLogsTest extends iTrustSeleniumTest {
    private HtmlUnitDriver driver;
    private StringBuffer verificationErrors = new StringBuffer();

    @Before
    public void setUp() throws Exception {
        super.setUp();
        gen.clearAllTables();
        gen.standardData();
        driver = new HtmlUnitDriver();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
    }

    @Test
    public void testAdminLogin() throws Exception {
        driver = (HtmlUnitDriver) login("9000000001", "pw");
        driver.setJavascriptEnabled(true);
        assertEquals("iTrust - Admin Home", driver.getTitle());
        driver.findElement(By.linkText("View Transaction Logs")).click();
        assertEquals("iTrust - View Transaction Logs", driver.getTitle());
    }

    @Test
    public void testTesterLogin() throws Exception {
        driver = (HtmlUnitDriver) login("9999999999", "pw");
        driver.setJavascriptEnabled(true);
        assertEquals("iTrust - Tester Home", driver.getTitle());
        driver.findElement(By.linkText("View Transaction Logs")).click();
        assertEquals("iTrust - View Transaction Logs", driver.getTitle());
    }

    @Test
    public void testAdminRegular() throws Exception {
        driver = (HtmlUnitDriver) login("9000000001", "pw");
        driver.setJavascriptEnabled(true);
        assertEquals("iTrust - Admin Home", driver.getTitle());
        driver.findElement(By.linkText("View Transaction Logs")).click();
        assertEquals("iTrust - View Transaction Logs", driver.getTitle());

        //fill form
        driver.findElement(By.name("startDate")).clear();
        driver.findElement(By.name("startDate")).sendKeys("05/10/2009");
        driver.findElement(By.name("endDate")).clear();
        driver.findElement(By.name("endDate")).sendKeys("10/10/2021");
		Select select;
		select = new Select (driver.findElement(By.name("mainRole")));
		select.selectByValue("All");
		select = new Select (driver.findElement(By.name("secondRole")));
		select.selectByValue("All");
		select = new Select (driver.findElement(By.name("transactionType")));
		select.selectByValue("All");

        //click button
        driver.findElement(By.id("viewReport")).click();

        //check to see if page shows info past 2009
        assertTrue(driver.getPageSource().contains("EMERGENCY_REPORT_VIEW"));
        assertTrue(driver.getPageSource().contains("Patient added to monitoring list"));
        assertTrue(driver.getPageSource().contains("2011-06-24 06:54:59.0"));

        //check to see pages does not show info before 2009
        assertFalse(driver.getPageSource().contains("Identified risk factors of chronic diseases"));
        assertFalse(driver.getPageSource().contains("2008-06-15 13:15:00.0"));
    }

    @Test
    public void testTesterRegular() throws Exception {
        driver = (HtmlUnitDriver) login("9999999999", "pw");
        driver.setJavascriptEnabled(true);
        assertEquals("iTrust - Tester Home", driver.getTitle());
        driver.findElement(By.linkText("View Transaction Logs")).click();
        assertEquals("iTrust - View Transaction Logs", driver.getTitle());

        //fill form
        driver.findElement(By.name("startDate")).clear();
        driver.findElement(By.name("startDate")).sendKeys("05/10/2009");
        driver.findElement(By.name("endDate")).clear();
        driver.findElement(By.name("endDate")).sendKeys("10/10/2021");
		Select select;
		select = new Select (driver.findElement(By.name("mainRole")));
		select.selectByValue("All");
		select = new Select (driver.findElement(By.name("secondRole")));
		select.selectByValue("All");
		select = new Select (driver.findElement(By.name("transactionType")));
		select.selectByValue("All");

        //click button
        driver.findElement(By.id("viewReport")).click();

        //check to see if page shows info past 2009
        assertTrue(driver.getPageSource().contains("EMERGENCY_REPORT_VIEW"));
        assertTrue(driver.getPageSource().contains("Patient added to monitoring list"));
        assertTrue(driver.getPageSource().contains("2011-06-24 06:54:59.0"));

        //check to see pages does not show info before 2009
        assertFalse(driver.getPageSource().contains("Identified risk factors of chronic diseases"));
        assertFalse(driver.getPageSource().contains("2008-06-15 13:15:00.0"));
    }

    @Test
    public void testAdminSelectMainRole() throws Exception {
        driver = (HtmlUnitDriver) login("9000000001", "pw");
        driver.setJavascriptEnabled(true);
        assertEquals("iTrust - Admin Home", driver.getTitle());
        driver.findElement(By.linkText("View Transaction Logs")).click();
        assertEquals("iTrust - View Transaction Logs", driver.getTitle());

        //fill form
        driver.findElement(By.name("startDate")).clear();
        driver.findElement(By.name("startDate")).sendKeys("05/10/2000");
        driver.findElement(By.name("endDate")).clear();
        driver.findElement(By.name("endDate")).sendKeys("10/10/2021");
		Select select;
		select = new Select (driver.findElement(By.name("mainRole")));
		select.selectByValue("hcp");
		select = new Select (driver.findElement(By.name("secondRole")));
		select.selectByValue("All");
		select = new Select (driver.findElement(By.name("transactionType")));
		select.selectByValue("All");

        //click view report button
        driver.findElement(By.id("viewReport")).click();

        //check to see if first five rows are only hcp
        assertTrue(driver.findElements(By.id("resultRow")).get(0).findElement(By.id("mainTD")).getText().contains("hcp"));
        assertTrue(driver.findElements(By.id("resultRow")).get(1).findElement(By.id("mainTD")).getText().contains("hcp"));
        assertTrue(driver.findElements(By.id("resultRow")).get(2).findElement(By.id("mainTD")).getText().contains("hcp"));
        assertTrue(driver.findElements(By.id("resultRow")).get(3).findElement(By.id("mainTD")).getText().contains("hcp"));
        assertTrue(driver.findElements(By.id("resultRow")).get(4).findElement(By.id("mainTD")).getText().contains("hcp"));

        //click summarize button
        driver.findElement(By.id("viewGraph")).click();

        //check to see if graphs are shown
        assertTrue(!driver.findElements(By.id("Graph_table")).isEmpty());
        assertTrue(!driver.findElements(By.id("chart_div")).isEmpty());
        assertTrue(!driver.findElements(By.id("chart_div2")).isEmpty());
        assertTrue(!driver.findElements(By.id("chart_div3")).isEmpty());
        assertTrue(!driver.findElements(By.id("chart_div4")).isEmpty());
    }

    @Test
    public void testAdminSelectSecondaryRole() throws Exception {
        driver = (HtmlUnitDriver) login("9000000001", "pw");
        driver.setJavascriptEnabled(true);
        assertEquals("iTrust - Admin Home", driver.getTitle());
        driver.findElement(By.linkText("View Transaction Logs")).click();
        assertEquals("iTrust - View Transaction Logs", driver.getTitle());

        //fill form
        driver.findElement(By.name("startDate")).clear();
        driver.findElement(By.name("startDate")).sendKeys("05/10/2000");
        driver.findElement(By.name("endDate")).clear();
        driver.findElement(By.name("endDate")).sendKeys("10/10/2021");
		Select select;
		select = new Select (driver.findElement(By.name("mainRole")));
		select.selectByValue("All");
		select = new Select (driver.findElement(By.name("secondRole")));
		select.selectByValue("patient");
		select = new Select (driver.findElement(By.name("transactionType")));
		select.selectByValue("All");

        //click view report button
        driver.findElement(By.id("viewReport")).click();

        //check to see if first five rows are only patient
        assertTrue(driver.findElements(By.id("resultRow")).get(0).findElement(By.id("secondTD")).getText().contains("patient"));
        assertTrue(driver.findElements(By.id("resultRow")).get(1).findElement(By.id("secondTD")).getText().contains("patient"));
        assertTrue(driver.findElements(By.id("resultRow")).get(2).findElement(By.id("secondTD")).getText().contains("patient"));
        assertTrue(driver.findElements(By.id("resultRow")).get(3).findElement(By.id("secondTD")).getText().contains("patient"));
        assertTrue(driver.findElements(By.id("resultRow")).get(4).findElement(By.id("secondTD")).getText().contains("patient"));

        //click summarize button
        driver.findElement(By.id("viewGraph")).click();

        //check to see if graphs are shown
        assertTrue(!driver.findElements(By.id("Graph_table")).isEmpty());
        assertTrue(!driver.findElements(By.id("chart_div")).isEmpty());
        assertTrue(!driver.findElements(By.id("chart_div2")).isEmpty());
        assertTrue(!driver.findElements(By.id("chart_div3")).isEmpty());
        assertTrue(!driver.findElements(By.id("chart_div4")).isEmpty());
    }

    @Test
    public void testAdminSelectType() throws Exception {
        driver = (HtmlUnitDriver) login("9000000001", "pw");
        driver.setJavascriptEnabled(true);
        assertEquals("iTrust - Admin Home", driver.getTitle());
        driver.findElement(By.linkText("View Transaction Logs")).click();
        assertEquals("iTrust - View Transaction Logs", driver.getTitle());

        //fill form
        driver.findElement(By.name("startDate")).clear();
        driver.findElement(By.name("startDate")).sendKeys("05/10/2000");
        driver.findElement(By.name("endDate")).clear();
        driver.findElement(By.name("endDate")).sendKeys("10/10/2021");
		Select select;
		select = new Select (driver.findElement(By.name("mainRole")));
		select.selectByValue("All");
		select = new Select (driver.findElement(By.name("secondRole")));
		select.selectByValue("All");
		select = new Select (driver.findElement(By.name("transactionType")));
		select.selectByValue("EMERGENCY_REPORT_VIEW");

        //click view report button
        driver.findElement(By.id("viewReport")).click();

        //check to see if first two rows are only EMERGENCY_REPORT_VIEW
        assertTrue(driver.findElements(By.id("resultRow")).get(0).findElement(By.id("typeTD")).getText().contains("EMERGENCY_REPORT_VIEW"));
        assertTrue(driver.findElements(By.id("resultRow")).get(1).findElement(By.id("typeTD")).getText().contains("EMERGENCY_REPORT_VIEW"));

        //click summarize button
        driver.findElement(By.id("viewGraph")).click();

        //check to see if graphs are shown
        assertTrue(!driver.findElements(By.id("Graph_table")).isEmpty());
        assertTrue(!driver.findElements(By.id("chart_div")).isEmpty());
        assertTrue(!driver.findElements(By.id("chart_div2")).isEmpty());
        assertTrue(!driver.findElements(By.id("chart_div3")).isEmpty());
        assertTrue(!driver.findElements(By.id("chart_div4")).isEmpty());
    }

    @Test
    public void testAdminIncorrectDate() throws Exception {
        driver = (HtmlUnitDriver) login("9000000001", "pw");
        driver.setJavascriptEnabled(true);
        assertEquals("iTrust - Admin Home", driver.getTitle());
        driver.findElement(By.linkText("View Transaction Logs")).click();
        assertEquals("iTrust - View Transaction Logs", driver.getTitle());

        //fill form
        driver.findElement(By.name("startDate")).clear();
        driver.findElement(By.name("startDate")).sendKeys("05/10/20000000");
        driver.findElement(By.name("endDate")).clear();
        driver.findElement(By.name("endDate")).sendKeys("10/10/2021");
		Select select;
		select = new Select (driver.findElement(By.name("mainRole")));
		select.selectByValue("All");
		select = new Select (driver.findElement(By.name("secondRole")));
		select.selectByValue("All");
		select = new Select (driver.findElement(By.name("transactionType")));
		select.selectByValue("All");

        //click view report button
        driver.findElement(By.id("viewReport")).click();

        //check to see if error message appears
        assertTrue(driver.getPageSource().contains("ERROR: Date must by in the format: MM/dd/yyyy"));

        //click summarize button
        driver.findElement(By.id("viewGraph")).click();

        //check to see if error message appears
        assertTrue(driver.getPageSource().contains("ERROR: Date must by in the format: MM/dd/yyyy"));
        
    }

    @Test
    public void testAdminImpossibleDate() throws Exception {
        driver = (HtmlUnitDriver) login("9000000001", "pw");
        driver.setJavascriptEnabled(true);
        assertEquals("iTrust - Admin Home", driver.getTitle());
        driver.findElement(By.linkText("View Transaction Logs")).click();
        assertEquals("iTrust - View Transaction Logs", driver.getTitle());

        //fill form
        driver.findElement(By.name("startDate")).clear();
        driver.findElement(By.name("startDate")).sendKeys("05/10/2030");
        driver.findElement(By.name("endDate")).clear();
        driver.findElement(By.name("endDate")).sendKeys("10/10/2021");
		Select select;
		select = new Select (driver.findElement(By.name("mainRole")));
		select.selectByValue("All");
		select = new Select (driver.findElement(By.name("secondRole")));
		select.selectByValue("All");
		select = new Select (driver.findElement(By.name("transactionType")));
		select.selectByValue("All");

        //click button
        driver.findElement(By.id("viewReport")).click();

        //check to see if error message appears
        assertTrue(driver.getPageSource().contains("ERROR: End Date cannot be before Start Date"));

        //click summarize button
        driver.findElement(By.id("viewGraph")).click();

        //check to see if error message appears
        assertTrue(driver.getPageSource().contains("ERROR: End Date cannot be before Start Date"));
    }

    @Test
    public void testAdminNoResults() throws Exception {
        driver = (HtmlUnitDriver) login("9000000001", "pw");
        driver.setJavascriptEnabled(true);
        assertEquals("iTrust - Admin Home", driver.getTitle());
        driver.findElement(By.linkText("View Transaction Logs")).click();
        assertEquals("iTrust - View Transaction Logs", driver.getTitle());

        //fill form
        driver.findElement(By.name("startDate")).clear();
        driver.findElement(By.name("startDate")).sendKeys("05/10/2000");
        driver.findElement(By.name("endDate")).clear();
        driver.findElement(By.name("endDate")).sendKeys("10/10/2021");
		Select select;
		select = new Select (driver.findElement(By.name("mainRole")));
		select.selectByValue("pha");
		select = new Select (driver.findElement(By.name("secondRole")));
		select.selectByValue("pha");
		select = new Select (driver.findElement(By.name("transactionType")));
		select.selectByValue("All");

        //click view report button
        driver.findElement(By.id("viewReport")).click();

        //check to see if error message appears
        assertTrue(driver.getPageSource().contains("There are no results for the criteria specified"));

        //fill form again
        driver.findElement(By.name("startDate")).clear();
        driver.findElement(By.name("startDate")).sendKeys("05/10/2000");
        driver.findElement(By.name("endDate")).clear();
        driver.findElement(By.name("endDate")).sendKeys("10/10/2021");
		select = new Select (driver.findElement(By.name("mainRole")));
		select.selectByValue("pha");
		select = new Select (driver.findElement(By.name("secondRole")));
		select.selectByValue("pha");
		select = new Select (driver.findElement(By.name("transactionType")));
		select.selectByValue("All");

        //click summarize button
        driver.findElement(By.id("viewGraph")).click();

        //check to see if error message appears
        assertTrue(driver.getPageSource().contains("There are no results for the criteria specified"));

    }

    @After
    public void tearDown() throws Exception {
        driver.quit();
        String verificationErrorString = verificationErrors.toString();
        if (!"".equals(verificationErrorString)) {
            fail(verificationErrorString);
        }
    }
}