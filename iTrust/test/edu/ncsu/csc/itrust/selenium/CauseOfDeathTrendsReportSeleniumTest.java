package edu.ncsu.csc.itrust.selenium;

import edu.ncsu.csc.itrust.enums.TransactionType;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.support.ui.Select;

public class CauseOfDeathTrendsReportSeleniumTest extends iTrustSeleniumTest {

    private HtmlUnitDriver driver;

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        gen.clearAllTables();
        gen.standardData();
    }

    public void testViewCODTrendsReports_AllPatientsAllGenders() throws Exception {
        driver = (HtmlUnitDriver)login("9000000000", "pw");

        // Click Diagnosis Trends
        driver.findElement(By.cssSelector("h2.panel-title")).click();
        driver.findElement(By.xpath("//div[@class='panel-body']/ul/li[11]")).click();
        driver.findElement(By.linkText("Cause-Of-Death Trends Report")).click();

        // View Cause of Death Trends Report
        assertTrue(driver.getCurrentUrl().equals(ADDRESS + "auth/hcp/causeOfDeathTrendsReport.jsp"));
        driver.findElement(By.name("startDate")).clear();
        driver.findElement(By.name("startDate")).sendKeys("01/01/2000");
        driver.findElement(By.name("endDate")).sendKeys("12/31/2020");
        driver.findElement(By.id("view_report")).click();
        assertTrue(driver.getCurrentUrl().equals(ADDRESS + "auth/hcp/causeOfDeathTrendsReport.jsp"));
        assertLogged(TransactionType.DEATH_TRENDS_VIEW, 9000000000L, 0L,
                "View cause-of-death trends report");

        WebElement table = driver.findElement(By.id("causeOfDeathTrendsTable"));
        String cause1 = table.findElements(By.tagName("td")).get(0).getText();
        double code1 = Double.parseDouble(table.findElements(By.tagName("td")).get(1).getText());
        int deaths1 = Integer.parseInt(table.findElements(By.tagName("td")).get(2).getText());

        assertEquals(cause1, "Influenza");
        assertEquals(code1, 487.00);
        assertEquals(deaths1, 5);

        String cause2 = table.findElements(By.tagName("td")).get(3).getText();
        double code2 = Double.parseDouble(table.findElements(By.tagName("td")).get(4).getText());
        int deaths2 = Integer.parseInt(table.findElements(By.tagName("td")).get(5).getText());

        assertEquals(cause2, "Tuberculosis of the lung");
        assertEquals(code2, 11.40);
        assertEquals(deaths2, 3);
    }

    public void testViewCODTrendsReports_AllPatientsOnlyMale() throws Exception {
        driver = (HtmlUnitDriver)login("9000000000", "pw");

        // Click Diagnosis Trends
        driver.findElement(By.cssSelector("h2.panel-title")).click();
        driver.findElement(By.xpath("//div[@class='panel-body']/ul/li[11]")).click();
        driver.findElement(By.linkText("Cause-Of-Death Trends Report")).click();

        // View Cause of Death Trends Report
        assertTrue(driver.getCurrentUrl().equals(ADDRESS + "auth/hcp/causeOfDeathTrendsReport.jsp"));
        new Select(driver.findElement(By.name("gender"))).selectByVisibleText("Male");
        driver.findElement(By.name("startDate")).clear();
        driver.findElement(By.name("startDate")).sendKeys("01/01/2000");
        driver.findElement(By.name("endDate")).sendKeys("12/31/2020");
        driver.findElement(By.id("view_report")).click();
        assertTrue(driver.getCurrentUrl().equals(ADDRESS + "auth/hcp/causeOfDeathTrendsReport.jsp"));
        assertLogged(TransactionType.DEATH_TRENDS_VIEW, 9000000000L, 0L,
                "View cause-of-death trends report");

        WebElement table = driver.findElement(By.id("causeOfDeathTrendsTable"));
        String cause1 = table.findElements(By.tagName("td")).get(0).getText();
        double code1 = Double.parseDouble(table.findElements(By.tagName("td")).get(1).getText());
        int deaths1 = Integer.parseInt(table.findElements(By.tagName("td")).get(2).getText());

        assertEquals(cause1, "Influenza");
        assertEquals(code1, 487.00);
        assertEquals(deaths1, 3);

        String cause2 = table.findElements(By.tagName("td")).get(3).getText();
        double code2 = Double.parseDouble(table.findElements(By.tagName("td")).get(4).getText());
        int deaths2 = Integer.parseInt(table.findElements(By.tagName("td")).get(5).getText());

        assertEquals(cause2, "Malaria");
        assertEquals(code2, 84.50);
        assertEquals(deaths2, 2);
    }

    public void testViewCODTrendsReports_AllPatientsOnlyFemale() throws Exception {
        driver = (HtmlUnitDriver)login("9000000003", "pw");

        // Click Diagnosis Trends
        driver.findElement(By.cssSelector("h2.panel-title")).click();
        driver.findElement(By.xpath("//div[@class='panel-body']/ul/li[11]")).click();
        driver.findElement(By.linkText("Cause-Of-Death Trends Report")).click();

        // View Cause of Death Trends Report
        assertTrue(driver.getCurrentUrl().equals(ADDRESS + "auth/hcp/causeOfDeathTrendsReport.jsp"));
        new Select(driver.findElement(By.name("gender"))).selectByVisibleText("Female");
        driver.findElement(By.name("startDate")).clear();
        driver.findElement(By.name("startDate")).sendKeys("01/01/2000");
        driver.findElement(By.name("endDate")).sendKeys("12/31/2020");
        driver.findElement(By.id("view_report")).click();
        assertTrue(driver.getCurrentUrl().equals(ADDRESS + "auth/hcp/causeOfDeathTrendsReport.jsp"));
        assertLogged(TransactionType.DEATH_TRENDS_VIEW, 9000000003L, 0L,
                "View cause-of-death trends report");

        WebElement table = driver.findElement(By.id("causeOfDeathTrendsTable"));
        String cause1 = table.findElements(By.tagName("td")).get(0).getText();
        double code1 = Double.parseDouble(table.findElements(By.tagName("td")).get(1).getText());
        int deaths1 = Integer.parseInt(table.findElements(By.tagName("td")).get(2).getText());

        assertEquals(cause1, "Influenza");
        assertEquals(code1, 487.00);
        assertEquals(deaths1, 2);

        String cause2 = table.findElements(By.tagName("td")).get(3).getText();
        double code2 = Double.parseDouble(table.findElements(By.tagName("td")).get(4).getText());
        int deaths2 = Integer.parseInt(table.findElements(By.tagName("td")).get(5).getText());

        assertEquals(cause2, "Tuberculosis of the lung");
        assertEquals(code2, 11.40);
        assertEquals(deaths2, 1);
    }

    public void testViewCODTrendsReports_MyPatientsAllGenders() throws Exception {
        driver = (HtmlUnitDriver)login("9000000003", "pw");

        // Click Diagnosis Trends
        driver.findElement(By.cssSelector("h2.panel-title")).click();
        driver.findElement(By.xpath("//div[@class='panel-body']/ul/li[11]")).click();
        driver.findElement(By.linkText("Cause-Of-Death Trends Report")).click();

        // View Cause of Death Trends Report
        assertTrue(driver.getCurrentUrl().equals(ADDRESS + "auth/hcp/causeOfDeathTrendsReport.jsp"));
        new Select(driver.findElement(By.name("patients"))).selectByVisibleText("My Patients");
        driver.findElement(By.name("startDate")).clear();
        driver.findElement(By.name("startDate")).sendKeys("01/01/2000");
        driver.findElement(By.name("endDate")).sendKeys("12/31/2020");
        driver.findElement(By.id("view_report")).click();
        assertTrue(driver.getCurrentUrl().equals(ADDRESS + "auth/hcp/causeOfDeathTrendsReport.jsp"));
        assertLogged(TransactionType.DEATH_TRENDS_VIEW, 9000000003L, 0L,
                "View cause-of-death trends report");

        WebElement table = driver.findElement(By.id("causeOfDeathTrendsTable"));
        String cause1 = table.findElements(By.tagName("td")).get(0).getText();
        double code1 = Double.parseDouble(table.findElements(By.tagName("td")).get(1).getText());
        int deaths1 = Integer.parseInt(table.findElements(By.tagName("td")).get(2).getText());

        assertEquals(cause1, "Influenza");
        assertEquals(code1, 487.00);
        assertEquals(deaths1, 4);

        String cause2 = table.findElements(By.tagName("td")).get(3).getText();
        double code2 = Double.parseDouble(table.findElements(By.tagName("td")).get(4).getText());
        int deaths2 = Integer.parseInt(table.findElements(By.tagName("td")).get(5).getText());

        assertEquals(cause2, "Diabetes with ketoacidosis");
        assertEquals(code2, 250.10);
        assertEquals(deaths2, 1);
    }

    public void testViewCODTrendsReports_MyPatientsOnlyMale() throws Exception {
        driver = (HtmlUnitDriver)login("9000000003", "pw");

        // Click Diagnosis Trends
        driver.findElement(By.cssSelector("h2.panel-title")).click();
        driver.findElement(By.xpath("//div[@class='panel-body']/ul/li[11]")).click();
        driver.findElement(By.linkText("Cause-Of-Death Trends Report")).click();

        // View Cause of Death Trends Report
        assertTrue(driver.getCurrentUrl().equals(ADDRESS + "auth/hcp/causeOfDeathTrendsReport.jsp"));
        new Select(driver.findElement(By.name("patients"))).selectByVisibleText("My Patients");
        new Select(driver.findElement(By.name("gender"))).selectByVisibleText("Male");
        driver.findElement(By.name("startDate")).clear();
        driver.findElement(By.name("startDate")).sendKeys("01/01/2000");
        driver.findElement(By.name("endDate")).sendKeys("12/31/2020");
        driver.findElement(By.id("view_report")).click();
        assertTrue(driver.getCurrentUrl().equals(ADDRESS + "auth/hcp/causeOfDeathTrendsReport.jsp"));
        assertLogged(TransactionType.DEATH_TRENDS_VIEW, 9000000003L, 0L,
                "View cause-of-death trends report");

        WebElement table = driver.findElement(By.id("causeOfDeathTrendsTable"));
        String cause1 = table.findElements(By.tagName("td")).get(0).getText();
        double code1 = Double.parseDouble(table.findElements(By.tagName("td")).get(1).getText());
        int deaths1 = Integer.parseInt(table.findElements(By.tagName("td")).get(2).getText());

        assertEquals(cause1, "Influenza");
        assertEquals(code1, 487.00);
        assertEquals(deaths1, 2);

        String cause2 = table.findElements(By.tagName("td")).get(3).getText();
        double code2 = Double.parseDouble(table.findElements(By.tagName("td")).get(4).getText());
        int deaths2 = Integer.parseInt(table.findElements(By.tagName("td")).get(5).getText());

        assertEquals(cause2, "Diabetes with ketoacidosis");
        assertEquals(code2, 250.10);
        assertEquals(deaths2, 1);
    }

    public void testViewCODTrendsReports_MyPatientsOnlyFemale() throws Exception {
        driver = (HtmlUnitDriver)login("9000000003", "pw");

        // Click Diagnosis Trends
        driver.findElement(By.cssSelector("h2.panel-title")).click();
        driver.findElement(By.xpath("//div[@class='panel-body']/ul/li[11]")).click();
        driver.findElement(By.linkText("Cause-Of-Death Trends Report")).click();

        // View Cause of Death Trends Report
        assertTrue(driver.getCurrentUrl().equals(ADDRESS + "auth/hcp/causeOfDeathTrendsReport.jsp"));
        new Select(driver.findElement(By.name("patients"))).selectByVisibleText("My Patients");
        new Select(driver.findElement(By.name("gender"))).selectByVisibleText("Female");
        driver.findElement(By.name("startDate")).clear();
        driver.findElement(By.name("startDate")).sendKeys("01/01/2000");
        driver.findElement(By.name("endDate")).sendKeys("12/31/2020");
        driver.findElement(By.id("view_report")).click();
        assertTrue(driver.getCurrentUrl().equals(ADDRESS + "auth/hcp/causeOfDeathTrendsReport.jsp"));
        assertLogged(TransactionType.DEATH_TRENDS_VIEW, 9000000003L, 0L,
                "View cause-of-death trends report");

        WebElement table = driver.findElement(By.id("causeOfDeathTrendsTable"));
        String cause1 = table.findElements(By.tagName("td")).get(0).getText();
        double code1 = Double.parseDouble(table.findElements(By.tagName("td")).get(1).getText());
        int deaths1 = Integer.parseInt(table.findElements(By.tagName("td")).get(2).getText());

        assertEquals(cause1, "Influenza");
        assertEquals(code1, 487.00);
        assertEquals(deaths1, 2);
    }

    public void testViewCODTrendsReports_EmptyDates() throws Exception {
        driver = (HtmlUnitDriver)login("9000000003", "pw");

        // Click Diagnosis Trends
        driver.findElement(By.cssSelector("h2.panel-title")).click();
        driver.findElement(By.xpath("//div[@class='panel-body']/ul/li[11]")).click();
        driver.findElement(By.linkText("Cause-Of-Death Trends Report")).click();

        // View Cause of Death Trends Report
        assertTrue(driver.getCurrentUrl().equals(ADDRESS + "auth/hcp/causeOfDeathTrendsReport.jsp"));
        driver.findElement(By.name("startDate")).clear();
        driver.findElement(By.name("startDate")).sendKeys("");
        driver.findElement(By.name("endDate")).sendKeys("");
        driver.findElement(By.id("view_report")).click();
        assertTrue(driver.getCurrentUrl().equals(ADDRESS + "auth/hcp/causeOfDeathTrendsReport.jsp"));
        assertLogged(TransactionType.DEATH_TRENDS_VIEW, 9000000003L, 0L,
                "View cause-of-death trends report");

        assertTrue(driver.getPageSource().contains("Enter dates in MM/dd/yyyy"));
    }

    public void testViewCODTrendsReports_InvalidDateFormat() throws Exception {
        driver = (HtmlUnitDriver)login("9000000003", "pw");

        // Click Diagnosis Trends
        driver.findElement(By.cssSelector("h2.panel-title")).click();
        driver.findElement(By.xpath("//div[@class='panel-body']/ul/li[11]")).click();
        driver.findElement(By.linkText("Cause-Of-Death Trends Report")).click();

        // View Cause of Death Trends Report
        assertTrue(driver.getCurrentUrl().equals(ADDRESS + "auth/hcp/causeOfDeathTrendsReport.jsp"));
        driver.findElement(By.name("startDate")).clear();
        driver.findElement(By.name("startDate")).sendKeys("01-01-2000");
        driver.findElement(By.name("endDate")).sendKeys("12-31-2020");
        driver.findElement(By.id("view_report")).click();
        assertTrue(driver.getCurrentUrl().equals(ADDRESS + "auth/hcp/causeOfDeathTrendsReport.jsp"));
        assertLogged(TransactionType.DEATH_TRENDS_VIEW, 9000000003L, 0L,
                "View cause-of-death trends report");

        assertTrue(driver.getPageSource().contains("Enter dates in MM/dd/yyyy"));
    }

    public void testViewCODTrendsReports_StartDateAfterEndDate() throws Exception {
        driver = (HtmlUnitDriver)login("9000000003", "pw");

        // Click Diagnosis Trends
        driver.findElement(By.cssSelector("h2.panel-title")).click();
        driver.findElement(By.xpath("//div[@class='panel-body']/ul/li[11]")).click();
        driver.findElement(By.linkText("Cause-Of-Death Trends Report")).click();

        // View Cause of Death Trends Report
        assertTrue(driver.getCurrentUrl().equals(ADDRESS + "auth/hcp/causeOfDeathTrendsReport.jsp"));
        driver.findElement(By.name("startDate")).clear();
        driver.findElement(By.name("startDate")).sendKeys("01/01/2021");
        driver.findElement(By.name("endDate")).sendKeys("12/31/2020");
        driver.findElement(By.id("view_report")).click();
        assertTrue(driver.getCurrentUrl().equals(ADDRESS + "auth/hcp/causeOfDeathTrendsReport.jsp"));
        assertLogged(TransactionType.DEATH_TRENDS_VIEW, 9000000003L, 0L,
                "View cause-of-death trends report");

        assertTrue(driver.getPageSource().contains("Start date must be before end date!"));
    }
}
