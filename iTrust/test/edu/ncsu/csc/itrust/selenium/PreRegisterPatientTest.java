package edu.ncsu.csc.itrust.selenium;

import org.openqa.selenium.*;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;

public class PreRegisterPatientTest extends iTrustSeleniumTest {
	
	protected WebDriver driver;
	
	@Override
	protected void setUp() throws Exception {
		super.setUp();
		gen.clearAllTables();
        gen.standardData();

        driver = new HtmlUnitDriver();
        driver.manage().timeouts().implicitlyWait(DEFAULT_TIMEOUT, TimeUnit.SECONDS);
    }

    private void goToPreRegister() {
        // Go to preregister page
        driver.get(ADDRESS);
        driver.findElement(By.id("preregister_link")).click();
    }

    public void testPreRegisterPatientButton() throws Exception {
        goToPreRegister();
        assertEquals("iTrust - PreRegister Patient", driver.getTitle());
    }

    public void testPreRegisterPatientSuccessRequired() throws Exception {
        goToPreRegister();

        // Fill the form
        driver.findElement(By.xpath("//input[@name='firstName']")).sendKeys("fname");
        driver.findElement(By.xpath("//input[@name='lastName']")).sendKeys("lname");
        driver.findElement(By.xpath("//input[@name='email']")).sendKeys("name@email.com");
        driver.findElement(By.xpath("//input[@name='password']")).sendKeys("Password123");
        driver.findElement(By.xpath("//input[@name='verifyPassword']")).sendKeys("Password123");

        // Submit
        driver.findElement(By.id("submit_preregister")).click();
        
        assertTrue(driver.findElement(By.xpath("//body")).getText().contains("New Pre-registered Patient Information"));
    }
    
    public void testPreRegisterPatientSuccessAll() throws Exception {
        goToPreRegister();

        // Fill the form
        driver.findElement(By.xpath("//input[@name='firstName']")).sendKeys("fname");
        driver.findElement(By.xpath("//input[@name='lastName']")).sendKeys("lname");
        driver.findElement(By.xpath("//input[@name='email']")).sendKeys("name@email.com");
        driver.findElement(By.xpath("//input[@name='password']")).sendKeys("Password123");
        driver.findElement(By.xpath("//input[@name='verifyPassword']")).sendKeys("Password123");
        driver.findElement(By.xpath("//input[@name='streetAddress1']")).sendKeys("1234");
        driver.findElement(By.xpath("//input[@name='streetAddress2']")).sendKeys("123 ave");
        driver.findElement(By.xpath("//input[@name='city']")).sendKeys("Urbana C");
        driver.findElement(By.xpath("//input[@name='zip']")).sendKeys("12345");
        driver.findElement(By.xpath("//input[@name='phone']")).sendKeys("1234567890");
        driver.findElement(By.xpath("//input[@name='height']")).sendKeys("6.3");
        driver.findElement(By.xpath("//input[@name='weight']")).sendKeys("180");
        driver.findElement(By.xpath("//input[@name='icName']")).sendKeys("ABC");
        driver.findElement(By.xpath("//input[@name='icAddress1']")).sendKeys("123");
        driver.findElement(By.xpath("//input[@name='icAddress2']")).sendKeys("123 RD");
        driver.findElement(By.xpath("//input[@name='icCity']")).sendKeys("Chicago");
        driver.findElement(By.xpath("//input[@name='icZip']")).sendKeys("54321");
        driver.findElement(By.xpath("//input[@name='icPhone']")).sendKeys("0001234567");
        driver.findElement(By.xpath("//input[@id='smoker_yes']")).click();
        
        
        // Submit
        driver.findElement(By.id("submit_preregister")).click();
        assertTrue(driver.findElement(By.xpath("//body")).getText().contains("New Pre-registered Patient Information"));
    }

    public void testMissingElements() {
        goToPreRegister();

        // Check that all elements have the required attribute
        assertTrue(
            Boolean.parseBoolean(driver.findElement(By.xpath("//input[@name='firstName']")).getAttribute("required"))
            && Boolean.parseBoolean(driver.findElement(By.xpath("//input[@name='lastName']")).getAttribute("required"))
            && Boolean.parseBoolean(driver.findElement(By.xpath("//input[@name='email']")).getAttribute("required"))
            && Boolean.parseBoolean(driver.findElement(By.xpath("//input[@name='password']")).getAttribute("required"))
            && Boolean.parseBoolean(driver.findElement(By.xpath("//input[@name='verifyPassword']")).getAttribute("required"))
        );

        // Make sure that the form does not submit
        driver.findElement(By.xpath("//input[@name='firstName']")).sendKeys("");
        driver.findElement(By.xpath("//input[@name='lastName']")).sendKeys("lname");
        driver.findElement(By.xpath("//input[@name='email']")).sendKeys("name@email.com");
        driver.findElement(By.xpath("//input[@name='password']")).sendKeys("Password123");
        driver.findElement(By.xpath("//input[@name='verifyPassword']")).sendKeys("Password123");

        driver.findElement(By.id("submit_preregister")).click();

        assertFalse(driver.findElement(By.xpath("//body")).getText().contains("New Pre-registered Patient Information"));
    }

    public void testInvalidName() {
        goToPreRegister();

        driver.findElement(By.xpath("//input[@name='firstName']")).sendKeys("123abc");
        driver.findElement(By.xpath("//input[@name='lastName']")).sendKeys("lname");
        driver.findElement(By.xpath("//input[@name='email']")).sendKeys("name@email.com");
        driver.findElement(By.xpath("//input[@name='password']")).sendKeys("Password123");
        driver.findElement(By.xpath("//input[@name='verifyPassword']")).sendKeys("Password123");

        driver.findElement(By.id("submit_preregister")).click();

        assertTrue(driver.findElement(By.xpath("//body")).getText().contains("This form has not been validated correctly"));
    }

    public void testInvalidEmailFormException() {
        goToPreRegister();

        driver.findElement(By.xpath("//input[@name='firstName']")).sendKeys("fname");
        driver.findElement(By.xpath("//input[@name='lastName']")).sendKeys("lname");
        driver.findElement(By.xpath("//input[@name='email']")).sendKeys("not-an-email");
        driver.findElement(By.xpath("//input[@name='password']")).sendKeys("Password123");
        driver.findElement(By.xpath("//input[@name='verifyPassword']")).sendKeys("Password123");

        driver.findElement(By.id("submit_preregister")).click();

        assertTrue(driver.findElement(By.xpath("//body")).getText().contains("This form has not been validated correctly"));
    }
    
    public void testNonMatchPasswords() {
        goToPreRegister();

        driver.findElement(By.xpath("//input[@name='firstName']")).sendKeys("fname");
        driver.findElement(By.xpath("//input[@name='lastName']")).sendKeys("lname");
        driver.findElement(By.xpath("//input[@name='email']")).sendKeys("not-an-email");
        driver.findElement(By.xpath("//input[@name='password']")).sendKeys("Password123");
        driver.findElement(By.xpath("//input[@name='verifyPassword']")).sendKeys("Password1234");

        driver.findElement(By.id("submit_preregister")).click();

        assertFalse(driver.findElement(By.xpath("//body")).getText().contains("This form has not been validated correctly"));
    }

    public void testLogIn() {
        goToPreRegister();

        // Pre-register a patient
        driver.findElement(By.xpath("//input[@name='firstName']")).sendKeys("fname");
        driver.findElement(By.xpath("//input[@name='lastName']")).sendKeys("lname");
        driver.findElement(By.xpath("//input[@name='email']")).sendKeys("name@email.com");
        driver.findElement(By.xpath("//input[@name='password']")).sendKeys("Password123");
        driver.findElement(By.xpath("//input[@name='verifyPassword']")).sendKeys("Password123");
        driver.findElement(By.id("submit_preregister")).click();

        // Get the MID
        String mid = driver.findElement(By.id("newMID")).getText();

        // Try and log in
        driver.findElement(By.id("j_username")).sendKeys(mid);
        driver.findElement(By.id("j_password")).sendKeys("Password123");
        driver.findElement(By.xpath("//input[@type='submit' and @value='Login']")).click();

        assertEquals("iTrust - Pre-Registered Patient Home", driver.getTitle());
    }

    public void testDuplicateEmailITrustException() {
        // Add a user with name@email.com
        goToPreRegister();
        driver.findElement(By.xpath("//input[@name='firstName']")).sendKeys("fname");
        driver.findElement(By.xpath("//input[@name='lastName']")).sendKeys("lname");
        driver.findElement(By.xpath("//input[@name='email']")).sendKeys("name@email.com");
        driver.findElement(By.xpath("//input[@name='password']")).sendKeys("Password123");
        driver.findElement(By.xpath("//input[@name='verifyPassword']")).sendKeys("Password123");
        driver.findElement(By.id("submit_preregister")).click();
        
        goToPreRegister();

        // Fill the form
        driver.findElement(By.xpath("//input[@name='firstName']")).sendKeys("otherfname");
        driver.findElement(By.xpath("//input[@name='lastName']")).sendKeys("otherlname");
        driver.findElement(By.xpath("//input[@name='email']")).sendKeys("name@email.com");
        driver.findElement(By.xpath("//input[@name='password']")).sendKeys("Password123");
        driver.findElement(By.xpath("//input[@name='verifyPassword']")).sendKeys("Password123");

        // Submit
        driver.findElement(By.id("submit_preregister")).click();
        
        assertTrue(driver.findElement(By.xpath("//body")).getText().contains("This email is already in use. Please use another email address."));
    }
}