package edu.ncsu.csc.itrust.selenium;

import edu.ncsu.csc.itrust.enums.TransactionType;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;

import java.util.concurrent.TimeUnit;

public class MessageFilterTest extends iTrustSeleniumTest {
	private WebDriver driver;
	private StringBuffer verificationErrors = new StringBuffer();

	@Before
	public void setUp() throws Exception {
		super.setUp();
		gen.standardData();
		driver = new HtmlUnitDriver();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	}

	@Test
	public void testEditFilter() throws Exception {

		// This logs us into iTrust and returns the HtmlUnitDriver for use in
		HtmlUnitDriver driver = (HtmlUnitDriver)login("9000000000", "pw");

		// Make sure we were able to log in
		assertEquals("iTrust - HCP Home", driver.getTitle());
		driver.findElement(By.cssSelector("h2.panel-title")).click();
		driver.findElement(By.linkText("Message Inbox")).click();
		assertLogged(TransactionType.INBOX_VIEW, 9000000000L, 0L, "");
		String text = driver.findElement(By.cssSelector("table.fancyTable")).getText();
		assertTrue("Text not found!", text.contains("Random Person"));
		assertTrue("Text not found!", text.contains("Andy Programmer"));

		driver.findElement(By.linkText("Edit Filter")).click();
		driver.findElementById("subject").sendKeys("Scratchy Throat");

		driver.findElement(By.xpath("//input[@name='save']")).click();
		text = driver.findElement(By.cssSelector("table.fancyTable")).getText();
		assertTrue("Text not found!", text.contains("Scratchy Throat"));
		assertFalse("Text found!", text.contains("Missed Appointment"));

	}

	@Test
	public void testFilterPersistence() throws Exception {

		// This logs us into iTrust and returns the HtmlUnitDriver for use in
		HtmlUnitDriver driver = (HtmlUnitDriver)login("9000000000", "pw");

		// Make sure we were able to log in
		assertEquals("iTrust - HCP Home", driver.getTitle());
		driver.findElement(By.cssSelector("h2.panel-title")).click();
		driver.findElement(By.linkText("Message Outbox")).click();
		assertLogged(TransactionType.OUTBOX_VIEW, 9000000000L, 0L, "");
		String text = driver.findElement(By.cssSelector("table.fancyTable")).getText();
		assertTrue("Text not found!", text.contains("Random Person"));
		assertTrue("Text not found!", text.contains("Andy Programmer"));

		driver.findElement(By.linkText("Edit Filter")).click();
		driver.findElementById("subject").sendKeys("Scratchy Throat");

		driver.findElement(By.xpath("//input[@name='save']")).click();
		driver.findElement(By.linkText("Edit Filter")).click();
		text = driver.findElementById("subject").getAttribute("value");
		assertTrue("Filters are not persistent from page to page", text.contains("Scratchy Throat"));

		driver.findElement(By.xpath("//a[@href='/iTrust/logout.jsp']")).click();
		assertEquals("iTrust - Login", driver.getTitle());

		// Check if it's persistent between sessions
		HtmlUnitDriver driver2 = (HtmlUnitDriver)login("9000000000", "pw");

		assertEquals("iTrust - HCP Home", driver2.getTitle());
		driver2.findElement(By.cssSelector("h2.panel-title")).click();
		driver2.findElement(By.linkText("Message Outbox")).click();

		// Don't insert anything, just check what's already inputted
		driver2.findElement(By.linkText("Edit Filter")).click();
		text = driver2.findElementById("subject").getAttribute("value");
		assertTrue("Filters are not persistent between sessions", text.contains("Scratchy Throat"));
	}

	@Test
	public void testSortMessages() throws Exception {

		// This logs us into iTrust and returns the HtmlUnitDriver for use in
		HtmlUnitDriver driver = (HtmlUnitDriver)login("9000000000", "pw");

		// Make sure we were able to log in
		assertEquals("iTrust - HCP Home", driver.getTitle());
		driver.findElement(By.cssSelector("h2.panel-title")).click();
		driver.findElement(By.linkText("Message Inbox")).click();
		assertLogged(TransactionType.INBOX_VIEW, 9000000000L, 0L, "");
		String text = driver.findElement(By.cssSelector("table.fancyTable")).getText();

		assertTrue("Text not found!", text.contains("Random Person"));
		assertTrue("Text not found!", text.contains("Andy Programmer"));

		driver.findElement(By.xpath("//option[text()='Name']")).click();
		driver.findElement(By.xpath("//option[text()='Ascending']")).click();
		driver.findElement(By.xpath("//input[@name='sort']")).click();

		text = driver.findElement(By.cssSelector("table.fancyTable")).getText();
		String first = text.split("Read")[0];

		// This should be the first in reverse alphabetical order
		assertTrue("Text not found!", first.contains("Random Person"));
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}
