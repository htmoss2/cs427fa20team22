package edu.ncsu.csc.itrust.selenium;

import java.util.concurrent.TimeUnit;

import org.junit.*;
import org.openqa.selenium.*;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;

public class ViewAllPreregisteredPatientsTest extends iTrustSeleniumTest {
	private WebDriver driver;
	private StringBuffer verificationErrors = new StringBuffer();

	@Before
	public void setUp() throws Exception {
		super.setUp();
		gen.standardData();
		gen.patient92();
		driver = new HtmlUnitDriver();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	}

	@Test
	public void testViewAllPreregisteredPatients() throws Exception {

		// This logs us into iTrust and returns the HtmlUnitDriver for use in
		// this case
		HtmlUnitDriver driver = (HtmlUnitDriver)login("9000000000", "pw");

		// Make sure we were able to log in
		assertEquals("iTrust - HCP Home", driver.getTitle());
		driver.findElement(By.cssSelector("h2.panel-title")).click();
		driver.findElement(By.linkText("View Pre-Registered Patients")).click();
		String text = driver.findElement(By.cssSelector("table.fTable")).getText();

		// Make sure the preregistered patients are shown in the list
	    assertTrue("Text not found!", text.contains("Brittany Franco"));
	    assertTrue("Text not found!", text.contains("1333 Who Cares Road Suite 102"));
	    assertTrue("Text not found!", text.contains("Raleigh NC"));
	    assertTrue("Text not found!", text.contains("brfranco@gmail.com"));
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
		String verificationErrorString = verificationErrors.toString();
		if (!"".equals(verificationErrorString)) {
			fail(verificationErrorString);
		}
	}

}
