package edu.ncsu.csc.itrust.selenium;

import java.util.concurrent.TimeUnit;

import edu.ncsu.csc.itrust.enums.TransactionType;
import org.junit.*;
import org.openqa.selenium.*;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;

public class ActivatePatientTest extends iTrustSeleniumTest {
	private WebDriver driver;
	private StringBuffer verificationErrors = new StringBuffer();

	@Before
	public void setUp() throws Exception {
		super.setUp();
		gen.standardData();
		gen.patient92();
		driver = new HtmlUnitDriver();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	}

	@Test
	public void testActivatePreregisteredPatient() throws Exception {
		HtmlUnitDriver driver = (HtmlUnitDriver)login("9000000000", "pw");

		// Make sure we were able to log in
		assertEquals("iTrust - HCP Home", driver.getTitle());
		driver.findElement(By.cssSelector("h2.panel-title")).click();
		driver.findElement(By.linkText("View Pre-Registered Patients")).click();
		String text = driver.findElement(By.cssSelector("table.fTable")).getText();

		// Make sure the preregistered patients show up in the preregistered patients list
		assertTrue("Text not found!", text.contains("Brittany Franco"));
		assertTrue("Text not found!", text.contains("1333 Who Cares Road Suite 102"));
		assertTrue("Text not found!", text.contains("Raleigh NC"));
		assertTrue("Text not found!", text.contains("brfranco@gmail.com"));

		// Ensure we can click on the patient and see more details
		driver.findElement(By.linkText("Brittany Franco")).click();
		text = driver.findElement(By.cssSelector("table.fTable")).getText();
		assertTrue("Text not found!", text.contains("Phone:919-971-0000"));
		assertTrue("Text not found!", text.contains("Email:brfranco@gmail.com"));
		assertTrue("Text not found!", text.contains("Address: 1333 Who Cares Road"));

		// Activate the preregistered patient
		driver.findElement(By.linkText("Activate")).click();
		text = driver.findElement(By.cssSelector("table.fTable")).getText();
		assertTrue("Text not found!", text.contains("type \"I UNDERSTAND\" into the box below and click the button"));
		driver.findElement(By.name("understand")).sendKeys("I UNDERSTAND");
		driver.findElement(By.name("action")).click();
		assertLogged(TransactionType.REREGISTERED_PATIENT_ACTIVATE, 9000000000L, 92L, "");

		// Ensure the patient gets the fully activated screen
		text = driver.findElement(By.cssSelector("span.iTrustMessage")).getText();
		assertTrue("Text not found!", text.contains("Patient Successfully Activated!"));
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
		String verificationErrorString = verificationErrors.toString();
		if (!"".equals(verificationErrorString)) {
			fail(verificationErrorString);
		}
	}

}
